"""Configuration for the compression to work properly across all platforms.

In windows, 7zip can be installed and available to use in command line using
the msi installer available on the 7-zip.org

In Linux, 7zip can be installed using pre-compiled package p7zip. In Linux,
it may be named as '7za'. '7z' may not be created, for example in CentOS 6.

In Mac OS, p7zip can be installed using brew. THe command available is called
`7z`.
"""
import sys


# Common configuration
config = {
}


if sys.platform == 'win32':
    config['7z'] = '7z'
elif sys.platform == 'linux':
    config['7z'] = '7za'
elif sys.platform == 'darwin':
    config['7z'] = '7z'
else:
    config['7z'] = '7z'
