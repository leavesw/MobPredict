"""This script conducts a HMM trial for entropy rate estimation.
"""
import os
import json
import numpy as np
import matplotlib.pyplot as plt
from mobpredict.model import MC
from mobpredict.compression import PPM
from mobpredict.compression import LZ77
from mobpredict.compression import LZMA
from mobpredict.compression import Archive
from mobpredict.entropy import entropy_actual
from mobpredict.entropy import entropy_rand
from mobpredict.entropy import entropy_uncorrelated
from mobpredict.entropy import entropyEstimator
from mobpredict.entropy import entropyMC
from mobpredict.entropy import estimate_predictability


def EstimateEntropyRate(dataFile, method):
    """Estimate the entropy rate based on observation stored in dataFile.

    Args:
        dataFile (str): Filename that stores the observation sequence.
        method (str): The method used to estimate the entropy rate. Possible
            values include 'ppm', 'lz77', 'lzma', 'random', 'uncorrelated',
            'est' or 'actual'.
    """
    if not os.path.isfile(dataFile):
        raise FileNotFoundError(
            'Data file %s not found. Absolute Path: %s.' % (
                dataFile, os.path.abspath(dataFile)
            )
        )
    if method in ['ppm', 'lz77', 'lzma']:
        # These are compression based method
        dictCompressors = {
            'ppm': PPM(),
            'lz77': LZ77(),
            'lzma': LZMA()
        }
        dataFileDir = os.path.dirname(dataFile)
        dataFileNoExt = os.path.splitext(os.path.basename(dataFile))[0]
        outFilename = os.path.join(
            dataFileDir, '%s_%s.7z' % (dataFileNoExt, method)
        )
        # Compress only if the compressed file does not exist.
        if not os.path.isfile(outFilename):
            dictCompressors[method].compress(dataFile, outFilename)
        # Compute compression-based entropy rate
        archive = Archive(outFilename)
        return archive.bitRate
    else:
        fp = open(dataFile, 'rb')
        data = fp.read()
        fp.close()
        if method == 'random':
            reachableSymbols = []
            for s in data:
                if s not in reachableSymbols:
                    reachableSymbols.append(s)
            return entropy_rand(reachableSymbols)
        elif method == 'uncorrelated':
            return entropy_uncorrelated(data)
        elif method == 'actual':
            return entropy_actual(data, max_length=100)


def LoadDataIndex(dataIndexFile):
    """Load data index dictionary from file.

    Args:
        dataIndexFile (str): Path to the data index JSON file.

    Returns:

    """
    if os.path.isfile(dataIndexFile):
        fp = open(dataIndexFile, 'r')
        dictData = json.load(fp)
        fp.close()
    else:
        dictData = {}
    return dictData


def SaveDataIndex(tag, length, filename, dataIndexFile):
    """Save structure of checkpoint files

    Args:
        tag (str): Data tag. `origin`, `lz77`, `lzma`, or `ppm`.
        length (int): Length of the data ponts.
        filename (str): Name of the checkpoint file.
    """
    dictData = LoadDataIndex(dataIndexFile)
    if tag not in dictData:
        dictData[tag] = {
            'length': [],
            'filename': []
        }
    dictData[tag]['length'].append(int(length))
    dictData[tag]['filename'].append(os.path.basename(filename))
    fp = open(dataIndexFile, 'w')
    json.dump(dictData, fp, indent=4)
    fp.close()
    return dictData


def GetCheckpointFileFromDataIndex(tag, length, dictData):
    """Get the checkpoint file identified by $tag and $lengthArray exists in the
    data index dictionary.

    Args:
        tag (str): Data tag, such as 'origin' and so on.
        length (int): Length of observation.
        dictData (dict): Data index dictonary.

    Returns:
        str, None: Returns the checkpoint filename, or None, if such checkpoint
            file does not exist.
    """
    if tag in dictData and length in dictData[tag]['length']:
        index = dictData[tag]['length'].index(length)
        return dictData[tag]['filename'][index]
    else:
        return None


def LoadEntropyRate(entropyRateFile):
    """Load entropy rate structure from JSON file.

    Args:
        entropyRateFile (str): The JSON file that stored the entropy rate.
    """
    if os.path.isfile(entropyRateFile):
        fp = open(entropyRateFile, 'r')
        dictEntropyRate = json.load(fp)
        fp.close()
    else:
        dictEntropyRate = {}
    return dictEntropyRate


def SaveEntropyRate(tag, length, entropyRate, predictability, entropyRateFile):
    """Save entropy rate to JSON file.

    Args:
        tag (str): Name of the method used to estimate entropy rate.
        length (int): Length of the observation.
        entropyRate (float): Estimated entropy rate.
        predictability (float): Upper bound of predictability based on Fano's
            inequality.
        entropyRateFile (str): The JSON file to store the entropy rates.
    """
    dictEntropy = LoadEntropyRate(entropyRateFile)
    if tag not in dictEntropy:
        dictEntropy[tag] = {
            'length': [],
            'entropy_rate': [],
            'predictability': []
        }
    dictEntropy[tag]['length'].append(length)
    dictEntropy[tag]['entropy_rate'].append(entropyRate)
    dictEntropy[tag]['predictability'].append(predictability)
    fp = open(entropyRateFile, 'w')
    json.dump(dictEntropy, fp, indent=4, sort_keys=True)
    fp.close()
    return dictEntropy


def GetEntropyRatePredictability(tag, length, dictEntropyRate):
    """Load entropy rate and predictability from saved JSON dictionary.

    Args:
        tag (str): Name of the method used to estimate entropy rate.
        length (int): Length of the observation.
        entropyRateFile (str): The JSON file to store the entropy rates.
    """
    if tag in dictEntropyRate and length in dictEntropyRate[tag]['length']:
        entry = dictEntropyRate[tag]
        index = entry['length'].index(length)
        return {
            'entropy_rate': entry['entropy_rate'][index],
            'predictability': entry['predictability'][index]
        }
    else:
        return None


def PlotEntropyRate(dictEntropyRate,
                    ref=None,
                    weekRef=None,
                    methodLabels=None,
                    filename=None):
    """Plot estimated entropy rate and predictability

    Args:
        dictEntropyRate (dict): Dictionary containing calculated entropy rate
            and predictability.
        ref (dict): The true entropy rate and predictability.
    """
    if methodLabels is None:
        methodLabels = {
            m: m for m in dictEntropyRate
        }
    fig, [ax1, ax2] = plt.subplots(
        nrows=1, ncols=2, figsize=(20, 8)
    )
    if ref is not None:
        ax1.axhline(y=ref['entropy_rate'], color='r')
        ax2.axhline(y=ref['predictability'], color='r')
    if weekRef is not None:
        for week in weekRef:
            ax1.axvline(x=weekRef[week]['index'], color='r')
            ax2.axvline(x=weekRef[week]['index'], color='r')
    for m in dictEntropyRate.keys():
        ax1.plot(
            dictEntropyRate[m]['length'],
            dictEntropyRate[m]['entropy_rate'],
            label=methodLabels[m]
        )
        # When plotting the predictability, we need to consider the cases
        # where there is not enough observation for entropy rate to be properly
        # estimated.
        ax2ValidIndices = [
            i for i in range(len(dictEntropyRate[m]['length']))
            if dictEntropyRate[m]['predictability'][i] is not None
        ]
        validLength = [
            dictEntropyRate[m]['length'][i] for i in ax2ValidIndices
        ]
        validPredictability = [
            dictEntropyRate[m]['predictability'][i] for i in ax2ValidIndices
        ]
        ax2.plot(
            validLength, validPredictability, label=methodLabels[m]
        )
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    ax1.set_xlabel('Length of the observation.')
    ax1.set_ylabel('Estimated entropy rate (bits)')
    ax1.grid(
        axis='both',
        which='major',
        linestyle='--',
        linewidth='0.5',
        color='gray'
    )
    ax1.grid(
        axis='both',
        which='minor',
        linestyle=':',
        linewidth='0.5',
        color='gray'
    )
    ax1.set_title('Estimated Entropy Rate')
    ax1.legend()
    ax2.set_xscale('log')
    ax2.set_xlabel('Length of the sequence')
    ax2.set_ylabel('Estimated entropy rate (bits)')
    ax2.grid(
        axis='both',
        which='major',
        linestyle='--',
        linewidth='0.5',
        color='gray'
    )
    ax2.grid(
        axis='both',
        which='minor',
        linestyle=':',
        linewidth='0.5',
        color='gray'
    )
    ax2.set_title(
        'Predictability'
    )
    ax2.legend()
    if weekRef is not None:
        ax1Lim = ax1.get_ylim()
        ax2Lim = ax2.get_ylim()
        for week in weekRef:
            ax1.text(weekRef[week]['index'], ax1Lim[1], weekRef[week]['label'],
                     rotation=90, horizontalalignment='right',
                     verticalalignment='top')
            ax2.text(weekRef[week]['index'], ax2Lim[1], weekRef[week]['label'],
                     rotation=90, horizontalalignment='right',
                     verticalalignment='top')
    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)


dataDirectoryPath = "data/mc2"
dataFilenameTemplate = "events_%d.data"
dataIndexFilename = os.path.join(dataDirectoryPath, 'data_index.json')
entropyRateFilename = os.path.join(dataDirectoryPath, 'entropy.json')
modelFilename = os.path.join(dataDirectoryPath, 'model.json')
estimationMethods = [
    'ppm', 'lz77', 'lzma', 'random', 'uncorrelated', 'est', 'mc'
]

if __name__ == "__main__":
    os.makedirs(dataDirectoryPath, exist_ok=True)
    lengthArray = [
        int(i) for i in np.round(np.logspace(1, 7, 20)).astype(np.int)
    ]
    dictData = LoadDataIndex(dataIndexFilename)
    if os.path.isfile(modelFilename):
        model = MC.load(modelFilename)
    else:
        model = MC(20, 2)
        model.randomize()
        model.save(modelFilename)
    # Firstly, generate Bernoulli trial file and checkpoint.
    if GetCheckpointFileFromDataIndex(
            tag='origin',
            length=lengthArray[-1],
            dictData=dictData
    ) is None:
        data = model.simulate(lengthArray[-1])
        for l in lengthArray:
            print('Generating observation of length %d' % l)
            filename = dataFilenameTemplate % l
            filename = os.path.join(dataDirectoryPath, filename)
            if not os.path.isfile(filename):
                fp = open(filename, 'wb')
                fp.write(bytes(data[:l]))
                fp.close()
            dictData = SaveDataIndex(
                tag='origin',
                length=l,
                filename=filename,
                dataIndexFile=dataIndexFilename
            )
    # Secondly, estimate entropy rate
    dictEntropy = LoadEntropyRate(entropyRateFilename)
    for m in estimationMethods:
        print(
            'Computing entropy rate and predictability with method %s.' % m
        )
        if m == 'est' or m == 'mc':
            # Entropy rate estimation can be accelerated by providing
            # Length array. To check if we have this saved, check for the
            # longest distance.
            l = lengthArray[-1]
            if GetEntropyRatePredictability(m, l, dictEntropy) is None:
                dataFile = os.path.join(
                    dataDirectoryPath,
                    GetCheckpointFileFromDataIndex(
                        tag='origin',
                        length=l,
                        dictData=dictData
                    )
                )
                if not os.path.isfile(dataFile):
                    raise FileNotFoundError(
                        'Data file %s not found. Absolute Path: %s.' % (
                            dataFile, os.path.abspath(dataFile)
                        )
                    )
                fp = open(dataFile, 'rb')
                data = fp.read()
                data = bytes(data)
                fp.close()
                if m == 'est':
                    entropyRates = entropyEstimator(data, lengthArray)
                elif m == 'mc':
                    entropyRates = entropyMC(
                        data, list(range(20)), lengthArray
                    )
                for l_id, l in enumerate(lengthArray):
                    dictEntropy = SaveEntropyRate(
                        tag=m,
                        length=l,
                        entropyRate=entropyRates[l_id],
                        predictability=estimate_predictability(
                            alphabetSize=20,
                            entropy=entropyRates[l_id]
                        ),
                        entropyRateFile=entropyRateFilename
                    )
        else:
            for l in lengthArray:
                if GetEntropyRatePredictability(m, l, dictEntropy) is None:
                    entropyRate = EstimateEntropyRate(
                        dataFile=os.path.join(
                            dataDirectoryPath,
                            GetCheckpointFileFromDataIndex(
                                tag='origin',
                                length=l,
                                dictData=dictData
                            )
                        ),
                        method=m
                    )
                    print(entropyRate)
                    predictability = estimate_predictability(
                        alphabetSize=20,
                        entropy=entropyRate
                    )
                    dictEntropy = SaveEntropyRate(
                        tag=m,
                        length=l,
                        entropyRate=entropyRate,
                        predictability=predictability,
                        entropyRateFile=entropyRateFilename
                    )

    # Finally plot the result
    PlotEntropyRate(
        dictEntropyRate=dictEntropy,
        ref={
            "entropy_rate": model.entropyRate(),
            'predictability': estimate_predictability(
                20, model.entropyRate()
            )
        },
        filename=os.path.join(dataDirectoryPath, 'predictability.pdf')
    )
