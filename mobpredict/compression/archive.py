"""This file reads metadata of a compressed file.
"""
import os
import re
import subprocess

class Archive:
    """Read archive information in to structure
    """
    def __init__(self, inFile, install_dir=None):
        self.install_dir = install_dir
        if self.install_dir is not None:
            self._7z_cmd = os.path.join(self.install_dir, '7z')
        else:
            self._7z_cmd = '7z'
        self._filename = inFile
        cmd = [self._7z_cmd, 't', inFile]
        sp = subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        data, errs = sp.communicate()
        for line in data.decode('utf-8').splitlines():
            phyMatch = re.match(r"""^Physical Size = (\d+)""", line)
            if phyMatch:
                self._phySize = int(phyMatch.groups()[0])
            headerMatch = re.match(r"""^Headers Size = (\d+)""", line)
            if headerMatch:
                self._headerSize = int(headerMatch.groups()[0])
            originMatch = re.match(r"""^Size:\s+(\d+)""", line)
            if originMatch:
                self._origSize = int(originMatch.groups()[0])
            methodMatch = re.match(r"""^Method = (\w+)""", line)
            if methodMatch:
                self._compressionMethod = methodMatch.groups()[0]
        self._archiveSize = self._phySize - self._headerSize
    
    @property
    def originSize(self):
        return self._origSize
    
    @property
    def headerSize(self):
        return self._headerSize
    
    @property
    def phySize(self):
        return self._phySize
    
    @property
    def compressSize(self):
        return self._archiveSize
    
    @property
    def bitRate(self):
        """Get bit rate of the archive
        
        Returns:
            float: Compression bit rate.
        """
        return self._archiveSize * 8 / self._origSize

if __name__ == '__main__':
    archive = Archive('test.7z')
    print('Compression Rate: %.2f bits' % archive.bitRate)
