"""High-order Markov Chain

The Markov property specifies that the probability of a state depends only on
the previous state. However, memory can be inserted into the Markov chain by
making the current state depend on a number of previous states.

A nth order Markov chain can be characterized by the following conditional
probability

$f(X_{n+1}|X_n, X_{n-1}, \dots, X_1)$
"""
import os
import json
import itertools
import numpy as np


class MC:
    def __init__(self, numStates, order=1):
        self.numStates = numStates
        self.order = order
        self.map = np.arange(numStates ** order, dtype=np.int).reshape([
            numStates for i in range(order)
        ])
        self.probabilityMatrix = np.zeros(
            (numStates ** order, numStates), dtype=np.float
        )

    def randomize(self, predictability=0.8, seed=0):
        self.probabilityMatrix = np.random.uniform(0, 1., size=(
            self.numStates ** self.order, self.numStates
        ))
        maxTransitionArray = np.random.uniform(
            2 * predictability - 1, 1, size=self.numStates ** self.order
        )
        maxTransitionChoice = np.random.choice(
            self.numStates, size=self.numStates ** self.order
        )
        for i in range(self.numStates ** self.order):
            tJ = maxTransitionChoice[i]
            tMax = maxTransitionArray[i]
            sumRow = np.sum(self.probabilityMatrix[i, :]) - \
                     self.probabilityMatrix[i, tJ]
            for j in range(self.numStates):
                self.probabilityMatrix[i, j] = \
                    self.probabilityMatrix[i, j] / sumRow * (1 - tMax)
            self.probabilityMatrix[i, tJ] = tMax

    def simulate(self, length, init_state=None, seed=None):
        """Simulate the output of the HMM

        Args:
            length (int): Simulation lengthArray.
            init_state (int, optional): Defaults to None. Initial hidden state.
                Randomly picked if not specified.
            seed (int, optional): Random seed.
        """
        if seed is not None:
            np.random.seed(seed)
        # Initial state
        if init_state is None:
            init_state = np.random.randint(0, self.numStates, size=self.order)
        obs_seq = np.zeros((length,), dtype=np.uint8)
        obs_seq[:self.order] = init_state
        for i in range(length - self.order):
            stIndex = self.map[tuple(obs_seq[i:i+self.order])]
            # obs_seq[self.order + i] = np.random.choice(
            #     self.numStates, p=self._probabilityMatrix[stIndex, :]
            # )
            # Use multinomial instead of choice. Suspected 40% better performance.
            # See: https://github.com/numpy/numpy/issues/7543
            obs_seq[self.order + i] = np.random.multinomial(
                1, self.probabilityMatrix[stIndex, :]
            ).argmax()
        return obs_seq

    def save(self, filename):
        """Save HMM parameters to file

        Args:
            filename (string): The file to save the model to.
        """
        fp = open(filename, 'w')
        obj = {
            'num_states': self.numStates,
            'order': self.order,
            'map': self.map.tolist(),
            'probability': self.probabilityMatrix.tolist(),
        }
        json.dump(obj, fp, indent=4)
        fp.close()

    @staticmethod
    def load(filename):
        if os.path.isfile(filename):
            fp = open(filename, 'r')
            obj = json.load(fp)
            fp.close()
            model = MC(obj['num_states'], order=obj['order'])
            model.map = np.array(obj['map'], dtype=np.int)
            model.probabilityMatrix = np.array(
                obj['probability'], dtype=np.float
            )
        else:
            model = None
        return model

    def stationaryStateDist(self):
        a = np.zeros(
            (self.numStates ** self.order, self.numStates ** self.order),
            dtype=np.float
        )
        for indices in itertools.product(
            list(range(self.numStates)), repeat=self.order
        ):
            startIndex = self.map[indices]
            for targetState in range(self.numStates):
                targetIndices = indices[1:] + tuple([targetState])
                targetIndex = self.map[targetIndices]
                a[startIndex, targetIndex] = self.probabilityMatrix[
                    startIndex, targetState
                ]
        a = a.T - np.eye(self.numStates ** self.order)
        a[0, :] = 1.
        b = np.zeros((self.numStates ** self.order,))
        b[0] = 1.
        return np.linalg.solve(a, b)

    def entropyRate(self):
        state_dist = self.stationaryStateDist()
        logProbability = np.ma.log2(self.probabilityMatrix)
        state_entropy = np.sum(
            self.probabilityMatrix * logProbability,
            axis=1
        )
        return - np.dot(state_dist, state_entropy)
