"""This file implements the calculation of the random entropy $S^{rand}$, the 
temporal uncorrelated entropy $S^{unc}$ and the actual entropy $S_i$ according
to Song, et. al. [1].

Reference:
[1] Song, C., Qu, Z., Blumm, N. and Barabási, A.L., 2010. Limits of 
predictability in human mobility. Science, 327(5968), pp.1018-1021.
[2] Kontoyiannis, I., Algoet, P.H., Suhov, Y.M. and Wyner, A.J., 1998.
Nonparametric entropy estimation for stationary processes and random fields,
with applications to English text. IEEE Transactions on Information Theory,
44(3), pp.1319-1327.
"""
import numpy as np
from scipy.optimize import bisect
from .model import HMM

def entropy_rand(alphabet):
    """Random entropy $S^{rand}$ equals $\log_2 N$ where $N$ is the size of the
    alphabet.
    
    Args:
        alphabet (list): List of symbols.

    Returns:
        float: random entropy (bits).
    """
    return np.log2(len(alphabet))


def entropy_uncorrelated(data):
    """Compute temporal-uncorrelated entropy $S^{unc}$.
    It is assumed that the variables observed in the time series have no
    temporal correlation, thus are drawn from the alphebat identically and
    independently. Thus, the temporal uncorrelated $S^{unc}$ equals to
    $\sum_{x in \mathcal{X}} - P(x) \log_2(x)$.
    
    Args:
        data (bytes): Byte array.
    """
    dictProb = {}
    for symbol in data:
        if symbol not in dictProb:
            dictProb[symbol] = 1.
        else:
            dictProb[symbol] += 1.
    length = len(data)
    for symbol in dictProb:
        dictProb[symbol] = dictProb[symbol] / length
    entropy = 0.
    for symbol in dictProb:
        entropy += -1 * dictProb[symbol] * np.log2(dictProb[symbol])
    return entropy


def entropy_actual(data, max_length=None):
    """Compute actual entropy $S_i$ according to Song, et. al. [1].
    The actual entropy is computed as $\sum_{T_i \subeq T} - P(T_i) 
    log_2(P(T_i))$. However, there is no theoretical guarantee that it equals
    to the entropy rate of the underlying stationary stochastic process.
    
    Args:
        data (bytes): Byte array
        max_length (int, optional): Defaults to None. Maximum lengthArray of
            subsequence to be used in entropy calculation.
    """
    data_length = len(data)
    if max_length is None:
        max_length = data_length
    dictProb = {}
    total = 0
    for i in range(data_length):
        for j in range(1, min(max_length, data_length - i)):
            total += 1
            subSeq = data[i:i+j]
            if subSeq in dictProb:
                dictProb[subSeq] += 1.
            else:
                dictProb[subSeq] = 1.
    for seq in dictProb:
        dictProb[seq] = dictProb[seq] / total
    entropy = 0.
    for seq in dictProb:
        entropy += -1 * dictProb[seq] * np.log2(dictProb[seq])
    return entropy


def entropy_est(data):
    """According to the supplement material to Song, et. al. [1], an entropy 
    estimator based on Lempel-Ziv can be stated as
    $S^{est} = (1/n \sum_i \Gamma_i)^{-1} ln n$, where $\Gamma_i$ is the
    lengthArray of the shortest substring starting at position $i$ which doesn't
    previously appear from position 1 to i-1. 
    """
    data_length = len(data)
    observedString = set()
    for i in range(data_length):
        g = data[i:i+1]
        j = 1
        while g in observedString and i + j < data_length:
            j += 1
            g = data[i:i+j]
        if g not in observedString:
            observedString.add(g)
    uniqueStringLength = 0
    for g in observedString:
        uniqueStringLength += len(g)
    return data_length / uniqueStringLength * np.log2(data_length)


def entropyEstimator(data, lengthArray=None):
    """Entropy rate estimators introduced in [2].

    type:
        0: $\frac{n * \log_2 n}{\sum_{i=1}^{n} \Lambda_i^n}$
        1: $n * \sum_{i=2}^{n} \frac{\log_2 i}{\Lambda_i^i}$
        2: $\frac{n * \log_2 n}{\sum_{i=1}^{n} \Lambda_i^i}$

    In the above equations, $\Lambda_i^n$ is the lengthArray of the shortest
    substring $X_i^{i+k-1}$ starting at position i that does not appear as a
    continuous substring of the previous $n$ symbols X_{i-n}^{i-1}.

    Args:
        data:
        lengthArray:
        type:

    Returns:
    """
    dataLength = len(data)
    observedString = set()
    if lengthArray is None:
        lengthArray = [dataLength]
    entropyRateArray = np.zeros((len(lengthArray), ), dtype=np.float)
    for i in range(dataLength):
        j = 1
        g = data[i:i+j]
        while g in observedString and i + j < dataLength:
            j += 1
            g = data[i:i+j]
        if g not in observedString:
            observedString.add(g)
            # Now log result into entropy array
            for l_id, l in enumerate(lengthArray):
                if i+j <= lengthArray[l_id]:
                    entropyRateArray[l_id] += j
    # Update the entropy rate
    lengthArray = np.array(lengthArray)
    entropyRateArray = lengthArray * np.log2(lengthArray) / entropyRateArray
    return entropyRateArray


def entropyMC(data, alphabet, lengthArray=None):
    """Calculating the entropy rate if the data is modeled as a Markov chain.

    The entropy rate of the MC can be used to compare with the actual entropy
    rate to estimate how well the data can be modeled by a simple MC.
    """
    dataLength = len(data)
    if lengthArray is None:
        lengthArray = [dataLength]
    numSymbols = len(alphabet)
    transitionMatrix = np.zeros((numSymbols, numSymbols), dtype=np.float)
    entropyRateArray = np.zeros((len(lengthArray), ), dtype=np.float)
    prevState = None
    j = 0
    for i in range(dataLength):
        curState = data[i]
        if prevState is None:
            prevState = curState
            continue
        transitionMatrix[prevState, curState] += 1
        prevState = curState
        if i == lengthArray[j] - 1:
            transition = transitionMatrix / np.sum(
                transitionMatrix, axis=1, keepdims=True
            )
            symbolMask = ~np.isnan(transition).any(axis=1)
            transition = transition[symbolMask, :][:, symbolMask]
            model = HMM(transition.shape[0], transition.shape[0])
            model.transition = transition
            model.emission = transition
            entropyRateArray[j] = model.entropyRate()
            j += 1
    return entropyRateArray


def estimate_predictability(alphabetSize, entropy, precision=0.01):
    """Determine the upper-bound of predictability based on Fano's inequality.

    The Fano's inequality states that:
    $- P(e) \log_2 P(e) - (1 - P(e)) \log_2 (1 - P(e)) + P(e) \log_2 (N-1)
    \geq H(\mathcal{X})$

    Args:
        alphabetSize (int): Alphabet Size (N)
        entropy (float): Estimated entropy rate of the underlying stochastic
            process.
        precision (float, optional): Percentage precision of the predictability.
    """
    # Calculate lower bound of channel error, $P(e)$, by dynamic programming.
    # if entropy > np.log2(alphabetSize):
        # If entropy is greater than the upperbound of error, which means the
        # estimated entropy is inaccurate. Return None.
    #   return None
    def FanoFunction(e):
        return e * np.log2(alphabetSize - 1) - \
               e * np.log2(e) - (1 - e) * np.log2(1 - e) - entropy

    # Note that the fano function reaches maximum at e = 1/AlphabetSize.
    # As a result, the upper limit should be below 1/AlphabetSize
    eMax = 1 - 1/alphabetSize
    if FanoFunction(0.0001) * FanoFunction(eMax) >= 0:
        if np.abs(FanoFunction(0.0001)) > np.abs(FanoFunction(eMax)):
            return 1 - eMax
        else:
            return 1 - 0.0001

    # # Initialization
    # e_prev = 0.9
    # e = 0.01
    # while np.abs(e - e_prev) / e_prev > precision:
    #     e_prev = e
    #     e = 1 / np.log2(alphabetSize - 1) * (
    #             entropy + e * np.log2(e) + (1 - e) * np.log2(1 - e)
    #     )
    pE, info = bisect(FanoFunction, 0.0001, eMax, full_output=True)
    # print(FanoFunction(pE))
    if not (0 <= pE <= 1):
        raise ValueError(
            'Predictability calculation does not converge. '
            'Entropy rate %.4f, alphabetSize %d' % (
                entropy, alphabetSize
            )
        )
    return 1 - pE