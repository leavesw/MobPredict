# Predictability of Human Mobility in Smart Environment

Intelligent services provided by smart environment, such as location-aware applications.