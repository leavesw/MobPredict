import os
import json
import matplotlib.pyplot as plt

def LoadEntropyRateAndProdictability(chkptPath, tbname):
    """Find calculated entropy rate and predictability of a given smart home.
    """
    if not os.path.isdir(chkptPath):
        raise NotADirectoryError('Folder %s not found.' % chkptPath)
    tbChkptPath = os.path.join(chkptPath, tbname)
    if not os.path.isdir(tbChkptPath):
        raise NotADirectoryError(
            'Checkpoint folder %s for smart home %s not found.' % (
                tbChkptPath, tbname
            )
        )
    entropyFilename = os.path.join(tbChkptPath, 'entropy.json')
    if not os.path.isfile(entropyFilename):
        raise FileNotFoundError(
            'Entropy rate and prediction result not found. Make sure the file '
            '%s for smart home %s exists.' % (entropyFilename, tbname)
        )
    summaryFilename = os.path.join(tbChkptPath, 'summary.json')
    if not os.path.isfile(summaryFilename):
        raise FileNotFoundError(
            'Dataset summary file not found. Make sure that the file '
            '%s for smart home %s exists' % (summaryFilename, tbname)
        )
    fp = open(entropyFilename, 'r')
    dictEntropy = json.load(fp)
    fp.close()
    fp = open(summaryFilename, 'r')
    dictSummary = json.load(fp)
    fp.close()
    # return {
    #     'num_sensors': dictSummary['num_sensors'],
    #     'num_weeks': dictSummary['num_weeks'],
    #     'week_boundaries': dictSummary['week_boundaries']
    # }
    return dictEntropy, dictSummary


def PlotEntropyRate(dictEntropyRate,
                    weekRef=None,
                    methodLabels=None,
                    filename=None):
    """Plot estimated entropy rate and predictability

    Args:
        dictEntropyRate (dict): Dictionary containing calculated entropy rate
            and predictability.
    """
    if methodLabels is None:
        methodLabels = {
            m: m for m in dictEntropyRate
        }
    fig, ax1 = plt.subplots(
        nrows=1, ncols=1, figsize=(6, 4)
    )
    if weekRef is not None:
        for week in weekRef:
            ax1.axvline(x=weekRef[week]['index'], color='r')
    # Plot the estimators first
    for m in ['lz77', 'lzma', 'ppm', 'est']:
        ax1.plot(
            dictEntropyRate[m]['length'],
            dictEntropyRate[m]['entropy_rate'],
            label=methodLabels[m]
        )
    # Plot the estimators first
    for m in ['mc', 'uncorrelated']:
        ax1.plot(
            dictEntropyRate[m]['length'],
            dictEntropyRate[m]['entropy_rate'],
            label=methodLabels[m]
        )
    # Plot the estimators first
    for m in ['random']:
        ax1.plot(
            dictEntropyRate[m]['length'],
            [
                dictEntropyRate[m]['entropy_rate'][-1]
                for l in dictEntropyRate[m]['length']
            ],
            label=methodLabels[m]
        )
    ax1.set_xscale('log')
    #ax1.set_yscale('log')
    ax1.set_xlabel('Number of sensor events')
    ax1.set_ylabel('Estimated entropy rate (bits)')
    ax1.grid(
        axis='both',
        which='major',
        linestyle='--',
        linewidth='0.5',
        color='gray'
    )
    ax1.grid(
        axis='both',
        which='minor',
        linestyle=':',
        linewidth='0.5',
        color='gray'
    )
    ax1.legend()
    if weekRef is not None:
        ax1Lim = ax1.get_ylim()
        for week in weekRef:
            ax1.text(weekRef[week]['index'], ax1Lim[1], weekRef[week]['label'],
                     rotation=90, horizontalalignment='right',
                     verticalalignment='top')
    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)


def PlotPredictability(dictEntropyRate,
                       weekRef=None,
                       methodLabels=None,
                       filename=None):
    """Plot estimated entropy rate and predictability

    Args:
        dictEntropyRate (dict): Dictionary containing calculated entropy rate
            and predictability.
    """
    if methodLabels is None:
        methodLabels = {
            m: m for m in dictEntropyRate
        }
    fig, ax2 = plt.subplots(
        nrows=1, ncols=1, figsize=(6, 4)
    )
    if weekRef is not None:
        for week in weekRef:
            ax2.axvline(x=weekRef[week]['index'], color='r')
    # Plot the estimators first
    for m in ['lz77', 'lzma', 'ppm', 'est']:
        # When plotting the predictability, we need to consider the cases
        # where there is not enough observation for entropy rate to be properly
        # estimated.
        ax2ValidIndices = [
            i for i in range(len(dictEntropyRate[m]['length']))
            if dictEntropyRate[m]['predictability'][i] is not None
        ]
        validLength = [
            dictEntropyRate[m]['length'][i] for i in ax2ValidIndices
        ]
        validPredictability = [
            dictEntropyRate[m]['predictability'][i] for i in ax2ValidIndices
        ]
        ax2.plot(
            validLength, validPredictability, label=methodLabels[m]
        )
    for m in ['mc', 'uncorrelated']:
        # When plotting the predictability, we need to consider the cases
        # where there is not enough observation for entropy rate to be properly
        # estimated.
        ax2ValidIndices = [
            i for i in range(len(dictEntropyRate[m]['length']))
            if dictEntropyRate[m]['predictability'][i] is not None
        ]
        validLength = [
            dictEntropyRate[m]['length'][i] for i in ax2ValidIndices
        ]
        validPredictability = [
            dictEntropyRate[m]['predictability'][i] for i in ax2ValidIndices
        ]
        ax2.plot(
            validLength, validPredictability, label=methodLabels[m]
        )
    # Plot the estimators first
    for m in ['random']:
        # When plotting the predictability, we need to consider the cases
        # where there is not enough observation for entropy rate to be properly
        # estimated.
        ax2ValidIndices = [
            i for i in range(len(dictEntropyRate[m]['length']))
            if dictEntropyRate[m]['predictability'][i] is not None
        ]
        validLength = [
            dictEntropyRate[m]['length'][i] for i in ax2ValidIndices
        ]
        validPredictability = [
            dictEntropyRate[m]['predictability'][-1] for i in ax2ValidIndices
        ]
        ax2.plot(
            validLength, validPredictability, label=methodLabels[m]
        )
    ax2.set_xscale('log')
    ax2.set_xlabel('Number of sensor events')
    ax2.set_ylabel('Predictability')
    ax2.grid(
        axis='both',
        which='major',
        linestyle='--',
        linewidth='0.5',
        color='gray'
    )
    ax2.grid(
        axis='both',
        which='minor',
        linestyle=':',
        linewidth='0.5',
        color='gray'
    )
    ax2.legend()
    if weekRef is not None:
        ax2Lim = ax2.get_ylim()
        for week in weekRef:
            ax2.text(weekRef[week]['index'], ax2Lim[1], weekRef[week]['label'],
                     rotation=90, horizontalalignment='right',
                     verticalalignment='top')
    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)


chkptPath = 'data/chkpt'
tbname = 'tm004'

estimationMethods = {
    'ppm': r'$H^{PPMD}$',
    'lz77': r'$H^{LZ77}$',
    'lzma': r'$H^{LZMA}$',
    'random': r'$H^{rand}$',
    'uncorrelated': r'$H^{unc}$',
    'mc': r'$H^{MC}$',
    'est': r'$H^{est}$'
}

weekGuidelines = {
    '1w': 1,
    '2w': 2,
    '3w': 3,
    '4w': 4,
    '2m': 10,
    '3m': 15,
    '6m': 26,
    '1yr': 52,
    '1.5yr': 78,
    '2yr': 104
}

if __name__ == "__main__":
    dictEntropy, dictSummary = LoadEntropyRateAndProdictability(
        chkptPath, tbname
    )
    # Build week reference first
    weekList = dictSummary['week_boundaries']
    weekRef = {}
    for week in weekGuidelines:
        if weekGuidelines[week] >= len(weekList) - 1:
            break
        weekRef[week] = {
            'label': week,
            'index': weekList[weekGuidelines[week] + 1]
        }
    tbChkptPath = os.path.join(chkptPath, tbname)
    PlotEntropyRate(
        dictEntropyRate=dictEntropy,
        weekRef=weekRef,
        methodLabels=estimationMethods,
        filename=os.path.join(
            tbChkptPath, '%s_entropy.pdf' % tbname
        )
    )
    PlotPredictability(
        dictEntropyRate=dictEntropy,
        weekRef=weekRef,
        methodLabels=estimationMethods,
        filename=os.path.join(
            tbChkptPath, '%s_predictability.pdf' % tbname
        )
    )


