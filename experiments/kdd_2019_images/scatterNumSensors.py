"""Scatter plot of predictability and entropy rate of each smart home, against
the number of sensors deployed in those smart homes.
"""
import os
import json
import matplotlib.pyplot as plt


def IsSingleResident(tbname):
    """Returns true if there is only one resident staying in the smart home.
    """
    return tbname not in {'hh107', 'hh121', 'tm004', 'kyoto', 'tulum', 'rw104',
                          'rw110', 'cairo', 'paris',
                          'atmo1', 'atmo2', 'atmo4', 'atmo5', 'atmo6', 'atmo7',
                          'atmo8', 'atmo9', 'atmo10'}


def HasPets(tbname):
    """Returns true if the smart home has pets.
    """
    return tbname in {'cairo', 'paris', 'rw103', 'milan'}


def LoadEntropyRateAndProdictability(chkptPath, tbname, method='est'):
    """Find calculated entropy rate and predictability of a given smart home.
    """
    if not os.path.isdir(chkptPath):
        raise NotADirectoryError('Folder %s not found.' % chkptPath)
    tbChkptPath = os.path.join(chkptPath, tbname)
    if not os.path.isdir(tbChkptPath):
        raise NotADirectoryError(
            'Checkpoint folder %s for smart home %s not found.' % (
                tbChkptPath, tbname
            )
        )
    entropyFilename = os.path.join(tbChkptPath, 'entropy.json')
    if not os.path.isfile(entropyFilename):
        raise FileNotFoundError(
            'Entropy rate and prediction result not found. Make sure the file '
            '%s for smart home %s exists.' % (entropyFilename, tbname)
        )
    summaryFilename = os.path.join(tbChkptPath, 'summary.json')
    if not os.path.isfile(summaryFilename):
        raise FileNotFoundError(
            'Dataset summary file not found. Make sure that the file '
            '%s for smart home %s exists' % (summaryFilename, tbname)
        )
    fp = open(entropyFilename, 'r')
    dictEntropy = json.load(fp)
    fp.close()
    if method not in dictEntropy:
        raise ValueError(
            'Method %s not implemented during the entropy rate estimation of '
            'smart home %s.' % (method, tbname)
        )
    entropyRate = dictEntropy[method]['entropy_rate'][-1]
    predictability = dictEntropy[method]['predictability'][-1]
    length = dictEntropy[method]['length'][-1]
    fp = open(summaryFilename, 'r')
    dictSummary = json.load(fp)
    fp.close()
    return {
        'entropy_rate': entropyRate,
        'predictability': predictability,
        'length': length,
        'num_sensors': dictSummary['num_sensors'],
        'num_weeks': dictSummary['num_weeks']
    }


def ScatterPlotPredictability(
        result, filename=None,
        noPetMarker='o', withPetMarker='*',
        srColor='r', mrColor='b',
        predictabilityTextOffset=(0.01, 0.003)
    ):
    """Scatter plot of predictability and entropy rate of all smart homes.
    """
    fig, ax2 = plt.subplots(
        nrows=1, ncols=1, figsize=(8, 8)
    )
    for tbname in result:
        if HasPets(tbname):
            m = withPetMarker
        else:
            m = noPetMarker
        if IsSingleResident(tbname):
            c = srColor
        else:
            c = mrColor
        ax2.scatter(
            result[tbname]['num_sensors'],
            result[tbname]['predictability'],
            c=c, marker=m
        )
        ax2.text(
            result[tbname]['num_sensors'] + predictabilityTextOffset[0],
            result[tbname]['predictability'] + predictabilityTextOffset[1],
            tbname,
            color=c
        )
    ax2.set_xlabel(r'Number of sensors')
    ax2.set_ylabel(r'Predictability $\Pi^{max}$')
    ax2.grid(
        axis='both',
        which='major',
        linestyle='--',
        linewidth='0.5',
        color='gray'
    )
    ax2.grid(
        axis='both',
        which='minor',
        linestyle=':',
        linewidth='0.5',
        color='gray'
    )
    legendHandlers = [
        plt.Line2D((0, 0), (0, 0), color=srColor, marker='o', linestyle=''),
        plt.Line2D((0, 0), (0, 0), color=mrColor, marker='o', linestyle=''),
        plt.Line2D((0, 0), (0, 0), color='k', marker=noPetMarker, linestyle=''),
        plt.Line2D((0, 0), (0, 0), color='k', marker=withPetMarker,
                   linestyle=''),
    ]
    legendLabels = [
        'Single Resident Homes',
        'Multi-resident Homes',
        'No Pet',
        'With Pets'
    ]
    ax2.legend(legendHandlers, legendLabels)
    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)


testbeds = [
    # Two or more residents
    'hh107', 'hh121', 'tm004', 'rw110',
    'atmo1', 'atmo2', 'atmo4', 'atmo6', 'atmo7',
    'atmo8', 'atmo9', 'atmo10',
    # Two residents + pets
    'cairo', 'paris',
    # Single resident + pets
    'rw103', 'milan',
    # Single resident
    'hh101', 'hh110',
    'aruba', 'hh102', 'hh103', 'hh104', 'hh105', 'hh106', 'hh108', 'hh109',
    'hh111', 'hh112', 'hh113', 'hh114', 'hh115', 'hh116', 'hh117', 'hh118',
    'hh119', 'hh120', 'hh122', 'hh123', 'hh124', 'hh125', 'hh126',
    'hh127', 'hh128', 'hh129', 'hh130'
]

chkptPath = 'data/chkpt'

if __name__ == '__main__':
    result = {}
    for tbname in testbeds:
        result[tbname] = LoadEntropyRateAndProdictability(
            chkptPath=chkptPath, tbname=tbname
        )
    fp = open(os.path.join(chkptPath, 'scatter_plot.json'), 'w')
    json.dump(result, fp, indent=4)
    fp.close()
    ScatterPlotPredictability(
        result,
        filename=os.path.join(chkptPath, 'scatterPredictabilitySensors.pdf')
    )
