"""This scripts calls `casas_db_import.py` to download smart home data for a list of CASAS smart home sites according
to the testbeds information acquired.
"""
import os
import json
import argparse
from dateutil.parser import parse
from datetime import datetime
from subprocess import Popen, PIPE
import multiprocessing


here = os.path.dirname(__file__)


def LoadSitesSummary(summaryFile):
    """Load CASAS Smart Home site summaries.

    Args:
        summaryFile: Smart home site information JSON file.

    Returns:
        Dictionary of smart home site indexed by site name.
    """
    if os.path.isfile(summaryFile):
        fp = open(summaryFile, 'r')
        siteInfoList = json.load(fp)
        fp.close()
        siteInfoDict = {}
        for site in siteInfoList:
            siteInfoDict[site['name']] = site
        return siteInfoDict
    else:
        raise FileNotFoundError(
            'Cannot find summary file for CASAS sites at %s. (Absolute path: %s)' %
            (summaryFile, os.path.abspath(summaryFile))
        )


def FetchSiteData(username, password, tbname, siteInfo, outputDir):
    """Fetch all recorded data for the specified smart home site.

    Args:
        username:
        password:
        tbname:
        siteInfo:

    Returns:
        None
    """
    if tbname not in siteInfo:
        raise IndexError('Smart Home site %s not found.' % tbname)
    startDate = parse(siteInfo[tbname]['firstEventUTC'])
    stopDate = siteInfo[tbname]['latestEventUTC']
    if stopDate == "":
        stopDate = datetime.utcnow()
    else:
        stopDate = parse(stopDate)
    arguments = [
        'python', os.path.join(here, 'casas_db_import.py'),
        '-u', username, '-p', password,
        '--start_date', startDate.strftime("%m/%d/%Y"),
        '--stop_date', stopDate.strftime("%m/%d/%Y"),
        'localhost', tbname, outputDir
    ]
    p = Popen(arguments, shell=True)
    p.communicate()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Load Datasets"
    )
    parser.add_argument('-i', '--info', type=str,
                        help='Site information json file')
    parser.add_argument('-H', '--host', type=str,
                        help='CASAS database server address (server:port)')
    parser.add_argument('-u', '--user', type=str,
                        help='CASAS database username')
    parser.add_argument('-p', '--password', type=str,
                        help='CASAS database password')
    parser.add_argument('-o', '--output',
                        help='Folder to save the datasets')
    parser.add_argument('-s', '--sites', type=str,
                        help='Sites to download, separated by comma (,)')
    parser.add_argument('-a', '--all', action='store_true',
                        help='Download all datasets')
    parser.add_argument('-P', '--parallel', type=int, default=0,
                        help='Number of parallel threads. Default to 4.')
    args = parser.parse_args()
    if ':' in args.host:
        dbHost, dbPort = args.host.split(':')
    else:
        dbHost = args.host
        dbPort = '5432'
    dbUser = args.user
    dbPassword = args.password
    outputPath = args.output
    numProcMax = args.parallel
    infoFilename = args.info
    downloadAll = args.all
    sites = str(args.sites).split(',')

    # Get all sites
    siteInfoDict = LoadSitesSummary(infoFilename)

    if downloadAll or len(sites) == 0:
        sites = list(siteInfoDict.keys())

    total = len(sites)

    # Make sure output path exists
    os.makedirs(outputPath, exist_ok=True)

    # Process array
    if numProcMax <= 0:
        pool = multiprocessing.Pool()
    else:
        pool = multiprocessing.Pool(processes=numProcMax)
    for tbname in sites:
        args = (dbUser, dbPassword, tbname, siteInfoDict, os.path.join(outputPath, tbname))
        pool.apply_async(FetchSiteData, args=args)
    pool.close()
    pool.join()
