import os
import json
import matplotlib.pyplot as plt
from mobpredict.model import MC
from mobpredict.entropy import estimate_predictability


def LoadEntropyRate(entropyRateFile):
    """Load entropy rate structure from JSON file.

    Args:
        entropyRateFile (str): The JSON file that stored the entropy rate.
    """
    if os.path.isfile(entropyRateFile):
        fp = open(entropyRateFile, 'r')
        dictEntropyRate = json.load(fp)
        fp.close()
    else:
        dictEntropyRate = {}
    return dictEntropyRate


def PlotEntropyRate(dictEntropyRate,
                    ref=None,
                    methodLabels=None,
                    filename=None):
    """Plot estimated entropy rate and predictability

    Args:
        dictEntropyRate (dict): Dictionary containing calculated entropy rate
            and predictability.
        ref (float): The true entropy rate and predictability.
    """
    if methodLabels is None:
        methodLabels = {
            m: m for m in dictEntropyRate
        }
    fig, ax1 = plt.subplots(
        nrows=1, ncols=1, figsize=(6, 4)
    )
    if ref is not None:
        ax1.axhline(y=ref, color='r', linestyle='--')
    # Plot the estimators first
    for m in ['lz77', 'lzma', 'ppm', 'est']:
        ax1.plot(
            dictEntropyRate[m]['length'],
            dictEntropyRate[m]['predictability'],
            label=methodLabels[m]
        )
    # Plot reference
    for m in ['mc', 'uncorrelated']:
        ax1.plot(
            dictEntropyRate[m]['length'],
            dictEntropyRate[m]['predictability'],
            label=methodLabels[m]
        )
    for m in ['random']:
        ax1.plot(
            dictEntropyRate[m]['length'],
            [
                dictEntropyRate[m]['predictability'][-1]
                for l in dictEntropyRate[m]['length']
            ],
            label=methodLabels[m]
        )
    ax1.set_xscale('log')
    #ax1.set_yscale('log')
    if ref is not None:
        ax1.text(
            ax1.get_xlim()[-1],
            ref + 0.01,
            r'Average Predictability',
            color='r',
            verticalalignment='bottom',
            horizontalalignment='right'
        )
    ax1.set_xlabel('Length of the observation')
    ax1.set_ylabel(r'Predictability $\Pi$')
    ax1.grid(
        axis='both',
        which='major',
        linestyle='--',
        linewidth='0.5',
        color='gray'
    )
    ax1.grid(
        axis='both',
        which='minor',
        linestyle=':',
        linewidth='0.5',
        color='gray'
    )
    ax1.legend()
    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)


dataDirectoryPath = "data/mc2"
dataFilenameTemplate = "events_%d.data"
dataIndexFilename = os.path.join(dataDirectoryPath, 'data_index.json')
entropyRateFilename = os.path.join(dataDirectoryPath, 'entropy.json')
modelFilename = os.path.join(dataDirectoryPath, 'model.json')
estimationMethods = {
    'ppm': r'$H^{PPMD}$',
    'lz77': r'$H^{LZ77}$',
    'lzma': r'$H^{LZMA}$',
    'random': r'$H^{rand}$',
    'uncorrelated': r'$H^{unc}$',
    'mc': r'$H^{MC}$',
    'est': r'$H^{est}$'
}


if __name__ == "__main__":
    dictEntropy = LoadEntropyRate(entropyRateFilename)
    model = MC.load(modelFilename)
    # Finally plot the result
    PlotEntropyRate(
        dictEntropyRate=dictEntropy,
        ref=estimate_predictability(model.numStates, model.entropyRate()),
        methodLabels=estimationMethods,
        filename=os.path.join(dataDirectoryPath, 'mc2_predictability.pdf')
    )
    print(model.entropyRate())
    print(estimate_predictability(model.numStates, model.entropyRate()))
