"""Scatter plot of predictability and entropy rate of each smart home, against
the number of sensors deployed in those smart homes.
"""
import os
import json
import argparse
from collections import OrderedDict
import matplotlib.pyplot as plt


def IsSingle(tbname, datasetNameMapDict):
  """Check if the dataset is recorded in a single-resident smart home.

  The information is provided by the name map. Any dataset starts with 'm' or
  'b' in the mapped name is recorded in a multi-resident smart home.

  Args:
    tbname: str, original name of the dataset
    datasetNameMapDict: dict, containing the name map of the datasets, indexed
      by their original names.

  Returns:
    True, if the dataset is recorded in a single-resident smart home;
    False, otherwise.
  """
  mappedName = datasetNameMapDict[tbname]
  if isinstance(mappedName, dict):
    mappedName = mappedName['label']
  if mappedName[0] == 'b' or mappedName[0] == 'm':
    return False
  else:
    return True


def HasPet(tbname, datasetNamedMapDict):
  """Check if the dataset is recorded in a smart home with pets.

  The information is provided by the name map. Any dataset starts with 'm' or
  'b' in the mapped name is recorded in a multi-resident smart home.

  Args:
    tbname: str, original name of the dataset
    datasetNameMapDict: dict, containing the name map of the datasets, indexed
      by their original names.

  Returns:
    True, if the dataset is recorded in a single-resident smart home;
    False, otherwise.
  """
  mappedName = datasetNamedMapDict[tbname]
  if isinstance(mappedName, dict):
    mappedName = mappedName['label']
  if mappedName[0] == 'b' or mappedName[0] == 'p':
    return True
  else:
    return False


def LoadDatasetNameMap(filename):
  """Load name map from a file.

  Args:
      filename: str, path to the smart home name map

  Returns:
    Ordered dictionary of mapped dataset names.
  """
  datasetNameMapDict = OrderedDict()
  fp = open(filename, 'r')
  for line in fp.readlines():
    line = line.strip()
    if len(line) == 0 or line[0] == '#':
      continue
    tokens = line.split()
    print(tokens)
    datasetNameMapDict[tokens[0]] = tokens[1]
  fp.close()
  return datasetNameMapDict


room_types = ['bedrooms', 'bathrooms', 'living rooms', 'kitchen',
              'dining', 'other']


def LoadDatasetRoomInformation(filename):
  """Load dataset testbed room information from file

  Args:
    filename: str, path to the smart home name

  Returns:
    Ordered dictionary of mapped dataset names and room information
  """
  datasetInfoMapDict = OrderedDict()
  fp = open(filename, 'r')
  for line in fp.readlines():
    line = line.strip().replace(',', '')
    if len(line) == 0 or line[0] == '#':
      continue
    tokens = line.split()
    print(tokens)
    if tokens[2] == '?':
      continue
    testbed_name = tokens[0]
    datasetInfoMapDict[testbed_name] = {
      'label': tokens[1]
    }
    datasetInfoMapDict[testbed_name]['size'] = int(tokens[2])
    total_rooms = 0
    for i, room in enumerate(room_types):
      datasetInfoMapDict[testbed_name][room] = int(tokens[3 + i])
      total_rooms += int(tokens[3 + i])
    datasetInfoMapDict[testbed_name]['num_rooms'] = total_rooms
  fp.close()
  return datasetInfoMapDict


def LoadSitesSummary(summaryFile):
  """Load CASAS Smart Home site summaries.

  Args:
    summaryFile: Smart home site information JSON file.

  Returns:
    Dictionary of smart home site indexed by site name.
  """
  if os.path.isfile(summaryFile):
    fp = open(summaryFile, 'r')
    siteInfoList = json.load(fp)
    fp.close()
    siteInfoDict = OrderedDict()
    for site in siteInfoList:
      siteInfoDict[site['name']] = site
    return siteInfoDict
  else:
    raise FileNotFoundError(
      'Cannot find summary file for CASAS sites at %s. (Absolute path: %s)' %
      (summaryFile, os.path.abspath(summaryFile))
    )


def LoadEntropyRateAndProdictability(chkptRoot, tbname, method='est'):
  """Find calculated entropy rate and predictability of a given smart home.

  Args:
    chkptRoot: str, path to the root directory of dataset checkpoints
    tbname: str, original name of the dataset
    method: str, methods used to calculate entropy rate ('est')

  Returns:
    dict, containing the information of the predictability of the dataset.
  """
  if not os.path.isdir(chkptRoot):
    raise NotADirectoryError('Folder %s not found.' % chkptRoot)
  tbChkptPath = os.path.join(chkptRoot, tbname)
  if not os.path.isdir(tbChkptPath):
    raise NotADirectoryError(
      'Checkpoint folder %s for smart home %s not found.' % (
        tbChkptPath, tbname
      )
    )
  entropyFilename = os.path.join(tbChkptPath, 'entropy.json')
  if not os.path.isfile(entropyFilename):
    raise FileNotFoundError(
      'Entropy rate and prediction result not found. Make sure the file '
      '%s for smart home %s exists.' % (entropyFilename, tbname)
    )
  summaryFilename = os.path.join(tbChkptPath, 'summary.json')
  if not os.path.isfile(summaryFilename):
    raise FileNotFoundError(
      'Dataset summary file not found. Make sure that the file '
      '%s for smart home %s exists' % (summaryFilename, tbname)
    )
  fp = open(entropyFilename, 'r')
  dictEntropy = json.load(fp)
  fp.close()
  if method not in dictEntropy:
    raise ValueError(
      'Method %s not implemented during the entropy rate estimation of '
      'smart home %s.' % (method, tbname)
    )
  entropyRate = dictEntropy[method]['entropy_rate'][-1]
  predictability = dictEntropy[method]['predictability'][-1]
  length = dictEntropy[method]['length'][-1]
  fp = open(summaryFilename, 'r')
  dictSummary = json.load(fp)
  fp.close()
  return {
    'entropy_rate': entropyRate,
    'predictability': predictability,
    'length': length,
    'num_sensors': dictSummary['num_sensors'],
    'num_weeks': dictSummary['num_weeks']
  }


def ScatterPlot(result, datasetNameMapDict,
                filename=None, useMappedName=True,
                noPetMarker='o', withPetMarker='*',
                srColor='r', mrColor='b',
                entropyTextOffset=(0.02, 0.02),
                predictabilityTextOffset=(0.01, 0.005)):
  """Scatter plot of predictability and entropy rate of all smart homes.
  """
  fig, [ax1, ax2] = plt.subplots(
    nrows=1, ncols=2, figsize=(20, 8)
  )
  ax1_texts = []
  ax2_texts = []
  for tbname in result:
    if HasPet(tbname, datasetNameMapDict):
      m = withPetMarker
    else:
      m = noPetMarker
    if IsSingle(tbname, datasetNameMapDict):
      c = srColor
    else:
      c = mrColor
    if useMappedName:
      tbNameLabel = datasetNameMapDict[tbname]
    else:
      tbNameLabel = tbname
    ax1.scatter(
      result[tbname]['num_sensors'],
      result[tbname]['entropy_rate'],
      c=c, marker=m
    )
    ax1_texts.append(ax1.text(
      result[tbname]['num_sensors'] + entropyTextOffset[0],
      result[tbname]['entropy_rate'] + entropyTextOffset[1],
      tbNameLabel, color=c
    ))
    ax2.scatter(
      result[tbname]['num_sensors'],
      result[tbname]['predictability'],
      c=c, marker=m
    )
    ax2_texts.append(ax2.text(
      result[tbname]['num_sensors'] + predictabilityTextOffset[0],
      result[tbname]['predictability'] + predictabilityTextOffset[1],
      tbNameLabel,
      color=c
    ))
  ax1.set_xlabel('Number of sensors deployed.')
  ax1.set_ylabel(r'Entropy Rate $S^{est}$ (bits)')
  ax1.grid(
    axis='both',
    which='major',
    linestyle='--',
    linewidth='0.5',
    color='gray'
  )
  ax1.grid(
    axis='both',
    which='minor',
    linestyle=':',
    linewidth='0.5',
    color='gray'
  )
  ax2.set_xlabel('Number of sensors deployed.')
  ax2.set_ylabel('Predictability $\Pi_max$')
  ax2.grid(
    axis='both',
    which='major',
    linestyle='--',
    linewidth='0.5',
    color='gray'
  )
  ax2.grid(
    axis='both',
    which='minor',
    linestyle=':',
    linewidth='0.5',
    color='gray'
  )
  legendHandlers = [
    plt.Line2D((0, 0), (0, 0), color=srColor, marker='o', linestyle=''),
    plt.Line2D((0, 0), (0, 0), color=mrColor, marker='o', linestyle=''),
    plt.Line2D((0, 0), (0, 0), color='k', marker=noPetMarker, linestyle=''),
    plt.Line2D((0, 0), (0, 0), color='k', marker=withPetMarker,
               linestyle=''),
  ]
  legendLabels = [
    'Single Resident Homes',
    'Multi-resident Homes',
    'No Pet',
    'With Pets'
  ]
  ax2.legend(legendHandlers, legendLabels)
  plt.tight_layout()
  if filename is None:
    plt.show()
  else:
    plt.savefig(filename)


def ScatterPlotVsObservation(
  result, datasetNameMapDict,
  filename=None, useMappedName=True,
  noPetMarker='o', withPetMarker='*',
  srColor='r', mrColor='b',
  entropyTextOffset=(0.02, 0.02),
  predictabilityTextOffset=(0.01, 0.005)
):
  """Scatter plot of predictability and entropy rate of all smart homes.
    """
  fig, [ax1, ax2] = plt.subplots(
    nrows=1, ncols=2, figsize=(20, 8)
  )
  for tbname in result:
    if HasPet(tbname, datasetNameMapDict):
      m = withPetMarker
    else:
      m = noPetMarker
    if IsSingle(tbname, datasetNameMapDict):
      c = srColor
    else:
      c = mrColor
    if useMappedName:
      tbNameLabel = datasetNameMapDict[tbname]
    else:
      tbNameLabel = tbname
    ax1.scatter(
      result[tbname]['num_weeks'],
      result[tbname]['entropy_rate'],
      c=c, marker=m
    )
    ax1.text(
      result[tbname]['num_weeks'] + entropyTextOffset[0],
      result[tbname]['entropy_rate'] + entropyTextOffset[1],
      tbNameLabel, color=c
    )
    ax2.scatter(
      result[tbname]['num_weeks'],
      result[tbname]['predictability'],
      c=c, marker=m
    )
    ax2.text(
      result[tbname]['num_weeks'] + predictabilityTextOffset[0],
      result[tbname]['predictability'] + predictabilityTextOffset[1],
      tbNameLabel,
      color=c
    )
  ax1.set_xlabel('Number of weeks in the observation.')
  ax1.set_ylabel(r'Entropy Rate $S^{est}$ (bits)')
  ax1.grid(
    axis='both',
    which='major',
    linestyle='--',
    linewidth='0.5',
    color='gray'
  )
  ax1.grid(
    axis='both',
    which='minor',
    linestyle=':',
    linewidth='0.5',
    color='gray'
  )
  ax2.set_xlabel('Number of weeks in the observation.')
  ax2.set_ylabel(r'Predictability $\Pi_max$')
  ax2.grid(
    axis='both',
    which='major',
    linestyle='--',
    linewidth='0.5',
    color='gray'
  )
  ax2.grid(
    axis='both',
    which='minor',
    linestyle=':',
    linewidth='0.5',
    color='gray'
  )
  legendHandlers = [
    plt.Line2D((0, 0), (0, 0), color=srColor, marker='o', linestyle=''),
    plt.Line2D((0, 0), (0, 0), color=mrColor, marker='o', linestyle=''),
    plt.Line2D((0, 0), (0, 0), color='k', marker=noPetMarker, linestyle=''),
    plt.Line2D((0, 0), (0, 0), color='k', marker=withPetMarker,
               linestyle=''),
  ]
  legendLabels = [
    'Single Resident Homes',
    'Multi-resident Homes',
    'No Pet',
    'With Pets'
  ]
  ax2.legend(legendHandlers, legendLabels)
  plt.tight_layout()
  if filename is None:
    plt.show()
  else:
    plt.savefig(filename)


def ScatterPlotVsSensorDensity(
  result, datasetInfoMapDict,
  densityScale='perRoom',
  filename=None, useMappedName=True,
  noPetMarker='o', withPetMarker='*',
  srColor='r', mrColor='b',
  entropyTextOffset=(0.02, 0.02),
  predictabilityTextOffset=(0.01, 0.005)
):
  """Scatter plot of predictability and entropy rate of all smart homes.

  Args:
    result: dict, entropy rate and predictability result
    datasetInfoMapDict: dict, indexed by dataset name
    densityScale: str, 'perRoom' or 'perSqFeet' for sensor density
    filename: str, path to save the image
    useMappedName: boolean, True if use mapped name, False to use original name
    noPetMarker: str, marker for testbeds with no pet, defalut to 'o'
    withPetMarker: str, marker for testbeds with pets, default to '*'
    srColor: str, color for single-resident house, default to 'r'
    mrColor: str, color for multi-resident house, default to 'b'
    entropyTextOffset: tuple of float, text label offset wrt plot
    predictabilityTextOffset: tuple of float, text label offset wrt plot

  Returns:
    figure, where the images are drawn
  """
  fig, [ax1, ax2] = plt.subplots(
    nrows=1, ncols=2, figsize=(20, 8)
  )
  for tbname in result:
    if tbname not in datasetInfoMapDict:
      continue
    if HasPet(tbname, datasetInfoMapDict):
      m = withPetMarker
    else:
      m = noPetMarker
    if IsSingle(tbname, datasetInfoMapDict):
      c = srColor
    else:
      c = mrColor
    if useMappedName:
      tbNameLabel = datasetInfoMapDict[tbname]['label']
    else:
      tbNameLabel = tbname
    x_value = result[tbname]['num_sensors']
    if densityScale == 'perRoom':
      x_value = float(x_value) / datasetInfoMapDict[tbname]['num_rooms']
    else:
      x_value = float(x_value) / datasetInfoMapDict[tbname]['size']
    ax1.scatter(
      x_value,
      result[tbname]['entropy_rate'],
      c=c, marker=m
    )
    ax1.text(
      x_value + entropyTextOffset[0],
      result[tbname]['entropy_rate'] + entropyTextOffset[1],
      tbNameLabel, color=c
    )
    ax2.scatter(
      x_value,
      result[tbname]['predictability'],
      c=c, marker=m
    )
    ax2.text(
      x_value + predictabilityTextOffset[0],
      result[tbname]['predictability'] + predictabilityTextOffset[1],
      tbNameLabel,
      color=c
    )
  if densityScale == 'perRoom':
    ax1.set_xlabel('Sensor Density (# sensors / room)')
  else:
    ax1.set_xlabel('Sensor Density (# sensors / sq feet)')
  ax1.set_ylabel(r'Entropy Rate $S^{est}$ (bits)')
  ax1.grid(
    axis='both',
    which='major',
    linestyle='--',
    linewidth='0.5',
    color='gray'
  )
  ax1.grid(
    axis='both',
    which='minor',
    linestyle=':',
    linewidth='0.5',
    color='gray'
  )
  if densityScale == 'perRoom':
    ax2.set_xlabel('Sensor Density (# sensors / room)')
  else:
    ax2.set_xlabel('Sensor Density (# sensors / sq feet)')
  ax2.set_ylabel(r'Predictability $\Pi_max$')
  ax2.grid(
    axis='both',
    which='major',
    linestyle='--',
    linewidth='0.5',
    color='gray'
  )
  ax2.grid(
    axis='both',
    which='minor',
    linestyle=':',
    linewidth='0.5',
    color='gray'
  )
  legendHandlers = [
    plt.Line2D((0, 0), (0, 0), color=srColor, marker='o', linestyle=''),
    plt.Line2D((0, 0), (0, 0), color=mrColor, marker='o', linestyle=''),
    plt.Line2D((0, 0), (0, 0), color='k', marker=noPetMarker, linestyle=''),
    plt.Line2D((0, 0), (0, 0), color='k', marker=withPetMarker,
               linestyle=''),
  ]
  legendLabels = [
    'Single Resident Homes',
    'Multi-resident Homes',
    'No Pet',
    'With Pets'
  ]
  ax2.legend(legendHandlers, legendLabels)
  plt.tight_layout()
  if filename is None:
    plt.show()
  else:
    plt.savefig(filename)


if __name__ == '__main__':
  parser = argparse.ArgumentParser(
    description='Predictability Scatter Plot'
  )
  parser.add_argument('-i', '--info', type=str,
                      help='Site information json file')
  parser.add_argument('-m', '--map', type=str,
                      help='Dataset map file')
  parser.add_argument('-r', '--room', type=str,
                      help='Dataset Room Information file')
  parser.add_argument('-s', '--sites', type=str, default='',
                      help='Sites to download, separated by comma (,)')
  parser.add_argument('-c', '--chkpt-root', type=str,
                      help='Root directory of checkpoints')
  parser.add_argument('-o', '--output', default='.',
                      help='Directory to store scatter plot files.')
  args = parser.parse_args()
  infoFilename = args.info
  mapFilename = args.map
  roomInfoFilename = args.room
  outputPath = args.output
  sites = str(args.sites)
  chkptRoot = args.chkpt_root

  # Get all sites
  siteInfoDict = LoadSitesSummary(infoFilename)
  datasetNameMapDict = LoadDatasetNameMap(mapFilename)
  datasetInfoMapDict = LoadDatasetRoomInformation(roomInfoFilename)

  # Sites to plot
  if len(sites) == 0:
    sites = list(siteInfoDict.keys())
  else:
    sites = sites.split(',')

  result = {}
  total_weeks = 0
  for tbname in sites:
    if tbname not in datasetNameMapDict:
      continue
    total_weeks += datasetNameMapDict[tbname]['num_weeks']
    try:
      result[tbname] = LoadEntropyRateAndProdictability(
        chkptRoot=chkptRoot, tbname=tbname
      )
    except Exception as error:
      print("Unexpected Error: ", error)
  # Create folder for scatter plot
  os.makedirs(outputPath, exist_ok=True)
  fp = open(os.path.join(outputPath, 'scatter_plot.json'), 'w')
  json.dump(result, fp, indent=2)
  fp.close()
  # ScatterPlot(result,
  #             filename=os.path.join(outputPath, 'scatter.svg'),
  #             datasetNameMapDict=datasetNameMapDict)
  # ScatterPlotVsObservation(
  #   result, filename=os.path.join(outputPath, 'scatterVsLength.svg'),
  #   datasetNameMapDict=datasetNameMapDict
  # )
  ScatterPlotVsSensorDensity(
    result, filename=os.path.join(outputPath, 'scatterVsDensityPSqFeet.svg'),
    densityScale='perSqFeet',
    datasetInfoMapDict=datasetInfoMapDict,
    entropyTextOffset=(0.0002, 0.02),
    predictabilityTextOffset=(0.0001, 0.005)
  )
  print(total_weeks)
