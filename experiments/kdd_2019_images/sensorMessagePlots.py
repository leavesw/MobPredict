import os
import dateutil.parser
from pycasas.data import CASASDataset


def LoadCasasDataset(checkpoint, datasetPath, sitePath):
    if os.path.isfile(checkpoint):
        dataset = CASASDataset.load(checkpoint)
    else:
        dataset = CASASDataset(
            datasetPath, site_dir=sitePath
        )
        dataset.enable_sensor_with_sensor_categories(
            ['Motion', 'Door', 'Item']
        )
        dataset.load_events(True)
        dataset.save(checkpoint)
    return dataset


chkptPath = 'data/chkpt'
tbname = 'tm004'
startDate = '2016/12/27 7:45:09 +08:00'
length = 12

if __name__ == "__main__":
    dataset = LoadCasasDataset(
        
    )

