""" Script: Import dataset from CASAS Database
"""
import os
import sys
import psycopg2
import pytz
import json
import dateutil.parser
import argparse

conn = None


def DbQuerySiteInfo(tbname):
    """Database query to obtain information of a specific smart home site.

    Args:
        tbname (:obj:`str`): Testbed name.

    Return:
        seven tuple of type string, string, bool, string, datetime,
        and datetime for name, description, activeness, created datetime,
        timezone, first event time and last event time.
    """
    cur = conn.cursor()
    cur.execute(
        "select tbname, description, active, timezone, first_event,"
        "latest_event from testbed where tbname='%s';" % tbname
    )
    rowlist = cur.fetchall()
    if len(rowlist) == 0:
        raise ValueError("No testbed named %s" % tbname)
    return rowlist[0]


def DbQuerySiteSensors(tbname):
    """Database query to obtain sensor information of a specific smart home
    site.

    Args:
        tbname (:obj:`str`): Testbed name.

    Return:
         Array of sensor dictionary.
    """
    sensor_lookup = {}
    sensor_list = []
    cur = conn.cursor()
    cur.execute(
        "select target, sensor_type, serial from detailed_all_sensors "
        "where tbname='%s';" % tbname)
    rowlist = cur.fetchall()
    for row in rowlist:
        if row[0] in sensor_lookup:
            sensor_lookup[row[0]]["types"].append(row[1])
            sensor_lookup[row[0]]["serial"].append(row[2])
        else:
            sensor = {
                "name": row[0],
                "types": [row[1]],
                "locX": 0.,
                "locY": 0.,
                "sizeX": 0.05,
                "sizeY": 0.02,
                "description": "",
                "tag": "",
                "serial": [row[2]]
            }
            sensor_list.append(sensor)
            sensor_lookup[row[0]] = sensor
    return sensor_list


def get_category(type_name):
    """Obtain sensor category depending on sensor type string.

    Sensor categories include Battery, Error, Radio, Motion, Door, LightSwitch,
    Light, Temperature, Item and Other.

    Args:
        type_name: sensor_type string obtained from database

    Returns:
        corresponding event/sensor categories
    """
    if 'Battery' in type_name:
        return 'Battery'
    if 'Radio_error' in type_name:
        return 'Error'
    if 'Radio' in type_name or 'Zigbee' in type_name:
        return 'Radio'
    if 'Motion' in type_name:
        return 'Motion'
    if 'Door' in type_name:
        return 'Door'
    if 'LightSwitch' in type_name:
        return 'LightSwitch'
    if 'Light' in type_name:
        return 'Light'
    if 'Temperature' in type_name or 'Thermostat' in type_name:
        return 'Temperature'
    if 'Item' in type_name:
        return 'Item'
    return 'Other'


def DbQuerySensorEvents(tbname, start_stamp, stop_stamp, site_timezone):
    """Obtain sensor events bounded by the start_stamp and stop_stamp.

    Args:
        tbname (:obj:`name`): Testbed name.
        start_stamp (:obj:`DateTime`): Start time stamp with timezone info.
        stop_stamp (:obj:`DateTime`): Stop time stamp with timezone info.
        site_timezone (:obj:`tzinfo`): Timezone information of the site.

    Returns:
        :obj:`tuple` of :obj:`list` of :obj:`str` that contains binary
            event, temperature events, light sensor events, radio events,
            state events, error events and other events.
    """
    bin_events = []
    temp_events = []
    light_events = []
    radio_events = []
    other_events = []
    error_events = []
    state_events = []
    cur = conn.cursor()
    cur.execute(
        "select stamp, target, message, sensor_type, category from "
        "detailed_all_events where tbname='%s' and "
        "stamp > '%s' and stamp < '%s';" % (
            tbname, str(start_stamp), str(stop_stamp)
        )
    )
    eventList = cur.fetchall()
    for event in eventList:
        eventTime = event[0].astimezone(site_timezone)
        sensor_name = event[1]
        sensor_message = event[2]
        sensor_type = event[3]
        event_category = event[4]
        event_time_string = eventTime.strftime("%m/%d/%Y %H:%M:%S.%f ") + \
                            eventTime.strftime("%z")[:3] + ":" + \
                            eventTime.strftime("%z")[3:]
        eventString = "%s,%s,%s,,,%s," % (
            event_time_string, sensor_name, sensor_message, sensor_type
        )
        category = get_category(sensor_type)
        if str(sensor_message).upper() in [
            'ON', 'OFF', 'ABSENT', 'PRESENT', 'OPEN', 'CLOSE'
        ] and category in ['Door', 'Motion', 'Item']:
            if event_category == 'state':
                state_events.append(eventString)
            else:
                bin_events.append(eventString)
        else:
            if category == "Temperature":
                temp_events.append(eventString)
            elif category == "Light":
                light_events.append(eventString)
            elif category == "Radio":
                radio_events.append(eventString)
            elif category == "Error":
                error_events.append(eventString)
            else:
                other_events.append(eventString)
    return bin_events, temp_events, light_events, radio_events, error_events, \
        state_events, other_events


def SaveSite(site, site_dir):
    """Save smart home site information to site directory

    Args:
        site: Dictionary including site information.
        site_dir: Directory to save site file.
    """
    os.makedirs(site_dir, exist_ok=True)
    site_file = os.path.join(site_dir, "site.json")
    fp = open(site_file, "w+")
    json.dump(site, fp, indent=2)
    fp.close()


def ImportDataset(name, tbname):
    dataset = {
        "name": name,
        "activities": [],
        "residents": [],
        "site": tbname,
    }
    return dataset


def SaveDataset(dataset, dataset_dir, bin_events, temp_events, light_events,
                radio_events, error_events, state_events, other_events):
    """ Save dataset to disk

    Args:
        dataset: Dict structure containing metadata about the dataset.
        dataset_dir: The directory to save dataset files
        bin_events: Binary events
        temp_events: Temperature events
        light_events: Light events
        radio_events: Radio events
        error_events: Error events
        state_events: State events
        other_events: Other sensor events
    """
    os.makedirs(dataset_dir, exist_ok=True)
    dataset_file = os.path.join(dataset_dir, "dataset.json")
    fp = open(dataset_file, "w+")
    json.dump(dataset, fp, indent=4)
    fp.close()
    event_file = os.path.join(dataset_dir, "events.csv")
    fp = open(event_file, "w+")
    fp.write('\n'.join(bin_events))
    fp.close()
    event_file = os.path.join(dataset_dir, "temperature.csv")
    fp = open(event_file, "w+")
    fp.write('\n'.join(temp_events))
    fp.close()
    event_file = os.path.join(dataset_dir, "light.csv")
    fp = open(event_file, "w+")
    fp.write('\n'.join(light_events))
    fp.close()
    event_file = os.path.join(dataset_dir, "radio.csv")
    fp = open(event_file, "w+")
    fp.write('\n'.join(radio_events))
    fp.close()
    event_file = os.path.join(dataset_dir, "error.csv")
    fp = open(event_file, "w+")
    fp.write('\n'.join(error_events))
    fp.close()
    event_file = os.path.join(dataset_dir, "state.csv")
    fp = open(event_file, "w+")
    fp.write('\n'.join(state_events))
    fp.close()
    event_file = os.path.join(dataset_dir, "other.csv")
    fp = open(event_file, "w+")
    fp.write('\n'.join(other_events))
    fp.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Load dataset from CASAS Database.")
    parser.add_argument('dbHost', type=str,
                        help='CASAS database server address (server:port)')
    parser.add_argument('-u', '--user', type=str,
                        help='CASAS database username')
    parser.add_argument('-p', '--password', type=str,
                        help='CASAS database password')
    parser.add_argument('site', type=str, help='CASAS testbed name to import')
    parser.add_argument('--siteonly', action='store_true',
                        help='Create site only')
    parser.add_argument('--start_date', type=str,
                        help='Start date (local) in format YYYY-MM-DD')
    parser.add_argument('--stop_date', type=str,
                        help='Stop date (local) in format YYYY-MM-DD')
    parser.add_argument('output_dir',
                        help='Folder to save the site and dataset')
    args = parser.parse_args()
    # Parse Host and Port
    if ':' in args.dbHost:
        host, port = args.dbHost.split(':')
    else:
        host = args.dbHost
        port = '5432'
    tbname = args.site
    user = args.user
    password = args.password
    # Connect to Database
    if user is None:
        print("Connect to CASAS database at %s:%s" % (host, port))
        conn = psycopg2.connect(host=host, port=port, database="smarthomedata")
    else:
        print("Connect to CASAS database at %s:%s as user %s" %
              (host, port, user)
              )
        conn = psycopg2.connect(host=host, port=port, database="smarthomedata",
                                user=user, password=password)

    # Mkdir
    os.makedirs(args.output_dir, exist_ok=True)

    try:
        print("Lookup site %s on database..." % tbname)
        site_info = DbQuerySiteInfo(tbname)

        print("Import site %s..." % tbname)
        # import site
        sensor_info = DbQuerySiteSensors(tbname)
        site = {
            "name": tbname,
            "floorplan": "placeholder.png",
            "sensors": sensor_info,
            "timezone": site_info[3],
        }
        site_final_path = os.path.join(args.output_dir, "site")
        os.makedirs(site_final_path, exist_ok=True)
        print("Save site %s to %s ..." % (tbname, site_final_path))
        SaveSite(site, site_final_path)

        # Get Dataset
        site_timezone = pytz.timezone(site_info[3])
        start_date = site_timezone.localize(
            dateutil.parser.parse(args.start_date))
        stop_date = site_timezone.localize(
            dateutil.parser.parse(args.stop_date))
        print("Import Data between %s and %s" % (
        start_date.isoformat(), stop_date.isoformat()))

        bin_events, temp_events, light_events, radio_events, error_events, \
        state_events, other_events = DbQuerySensorEvents(
            tbname, start_date, stop_date, site_timezone
        )

        dataset = {
            "name": tbname + "_" + start_date.strftime(
                "%y%m%d") + "_" + stop_date.strftime("%y%m%d"),
            "activities": [],
            "residents": [],
            "site": tbname,
        }

        dataset_final_path = os.path.join(args.output_dir, "data")
        os.makedirs(dataset_final_path, exist_ok=True)

        print(
            "Save dataset %s to %s" % (dataset["name"], dataset_final_path))
        SaveDataset(dataset, dataset_final_path, bin_events, temp_events,
                    light_events, radio_events, error_events, state_events,
                    other_events)
    except:
        print("Unexpected error:", sys.exc_info()[0])
    conn.close()
