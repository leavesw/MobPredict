import os
import json
import numpy as np


class HMM:
    """Hidden Markov Model
    """
    def __init__(self, num_states, num_observations):
        """Initialize a Hidden Markov Model structure.
        
        Args:
            num_states (int): Number of hidden states.
            num_observations (int): Number of observable items.
        """
        self._num_states = num_states
        self._num_observations = num_observations
        self._transition = np.zeros((num_states, num_states), np.float)
        self._emission = np.zeros((num_states, num_observations), np.float)
    
    def randomize(self, avgEmission=0.85, avgTransition=0.7, seed=None):
        """Randomly generate the transition and prediction probability with
        a target predictability.
        """
        if seed is not None:
            np.random.seed(seed)
        maxTransitionArray = np.random.uniform(
            2 * avgTransition - 1, 1, size=self._num_states
        )
        maxTransitionChoice = np.random.choice(
            self._num_states, self._num_states
        )
        maxEmissionArray = np.random.uniform(
            2 * avgEmission - 1, 1, size=self._num_states
        )
        maxEmissionChoice = np.random.choice(
            self._num_observations, self._num_states
        )
        self._transition = np.random.uniform(0, 1, size=self._transition.shape)
        self._emission = np.random.uniform(0, 1, size=self._emission.shape)
        for i in range(self._num_states):
            tJ = maxTransitionChoice[i]
            tMax = maxTransitionArray[i]
            sumRow = np.sum(self._transition[i, :]) - self._transition[i, tJ]
            for j in range(self._num_states):
                self._transition[i, j] = self._transition[i, j] / sumRow * \
                                         (1 - tMax)
            self._transition[i, tJ] = tMax
            eJ = maxEmissionChoice[i]
            eMax = maxEmissionArray[i]
            sumRow = np.sum(self._emission[i, :]) - self._emission[i, eJ]
            for j in range(self._num_observations):
                self._emission[i, j] = self._emission[i, j] / sumRow * \
                                       (1 - eMax)
            self._emission[i, eJ] = eMax
    
    @property
    def transition(self):
        """ Return transition matrix
        Returns:
            numpy.array: transition matrix.
        """
        return self._transition
    
    @transition.setter
    def transition(self, val):
        _transition = np.array(val, dtype=np.float)
        if _transition.shape == (self._num_states, self._num_states):
            self._transition = _transition
        else:
            raise ValueError(self._transition)

    @property
    def emission(self):
        return self._emission

    @emission.setter
    def emission(self, val):
        _emission = np.array(val, dtype=np.float)
        if _emission.shape == (self._num_states, self._num_observations):
            self._emission = _emission
        else:
            raise ValueError(self._emission)

    def simulate(self, length, init_state=None, seed=None):
        """Simulate the output of the HMM
        
        Args:
            length (int): Simulation lengthArray.
            init_state (int, optional): Defaults to None. Initial hidden state. Randomly picked if not specified.
            seed (int, optional): Random seed.
        """
        if seed is not None:
            np.random.seed(seed)
        # Initial state
        if init_state is None or init_state >= self._num_states:
            init_state = np.random.randint(0, self._num_states)
        st = init_state
        obs_seq = np.zeros((length, ), dtype=np.uint8)
        st_seq = np.zeros((length, ), dtype=np.uint8)
        for i in range(length):
            st_seq[i] = st
            # obs_seq[i] = np.random.choice(self._num_observations, p=self._emission[st, :])
            # st = np.random.choice(self._num_states, p=self._transition[st, :])
            # Use multinomial instead of choice. Suspected 40% better performance.
            # See: https://github.com/numpy/numpy/issues/7543
            obs_seq[i] = np.random.multinomial(
                1, self._emission[st, :]
            ).argmax()
            st = np.random.multinomial(1, self._transition[st, :]).argmax()
        return obs_seq, st_seq

    def save(self, filename):
        """Save HMM parameters to file
        
        Args:
            filename (string): The file to save the model to.
        """
        fp = open(filename, 'w')
        obj = {
            'num_states': self._num_states,
            'num_observations': self._num_observations,
            'transition': self._transition.tolist(),
            'emission': self._emission.tolist(),
        }
        json.dump(obj, fp, indent=4)
        fp.close()

    @staticmethod
    def load(filename):
        if os.path.isfile(filename):
            fp = open(filename, 'r')
            obj = json.load(fp)
            fp.close()
            hmm = HMM(obj['num_states'], obj['num_observations'])
            hmm.transition = obj['transition']
            hmm.emission = obj['emission']
        else:
            hmm = None
        return hmm

    def stateEntropy(self, state):
        _dist = self._emission[state, :]
        # Must take care of 0 probability cells.
        _dist = _dist[_dist != 0]
        return - np.sum(_dist * np.log2(_dist))

    def stationaryStateDist(self):
        a = np.copy(self._transition.T)
        a = a - np.eye(self._num_states)
        a[0, :] = 1.
        b = np.zeros((self._num_states, ))
        b[0] = 1.
        return np.linalg.solve(a, b)
    
    def entropyRate(self):
        state_dist = self.stationaryStateDist()
        state_entropy = np.array([
            self.stateEntropy(state) for state in range(self._num_states)
        ])
        return np.dot(state_dist, state_entropy)
