import os
import json
import argparse
import multiprocessing
from pycasas.data import CASASDataset
from collections import OrderedDict


def LoadSitesSummary(summaryFile):
  """Load CASAS Smart Home site summaries.

  Args:
    summaryFile: Smart home site information JSON file.

  Returns:
    Dictionary of smart home site indexed by site name.
  """
  if os.path.isfile(summaryFile):
    fp = open(summaryFile, 'r')
    siteInfoList = json.load(fp)
    fp.close()
    siteInfoDict = OrderedDict()
    for site in siteInfoList:
      siteInfoDict[site['name']] = site
    return siteInfoDict
  else:
    raise FileNotFoundError(
      'Cannot find summary file for CASAS sites at %s. (Absolute path: %s)' %
      (summaryFile, os.path.abspath(summaryFile))
    )


def LoadCasasDataset(checkpoint, datasetPath):
  """Load CASAS dataset from checkpoints or folder containing original data

  Args:
      checkpoint: str, path to dataset checkpoint
  """
  if os.path.isfile(checkpoint):
    dataset = CASASDataset.load(checkpoint)
  else:
    dataset = CASASDataset(datasetPath)
    dataset.enable_sensor_with_sensor_categories(
      ['Motion', 'Door', 'Item']
    )
    dataset.load_events(True)
    dataset.save(checkpoint)
  return dataset


def constructDictDataSources(datasetList,
                             chkptRoot,
                             datasetRoot):
  """Construct a dictionary containing file names for checkpoints, dataset
  directories, etc.

  Args:
    datasetList: list of dataset names
    chkptRoot: path of the root directory for checkpoint files
    datasetRoot: path of the root directory where the dataset are stored

  Returns:
    A dictionary including metadata for dataset files, indexed by dataset name.
  """
  datasetSourcesDict = OrderedDict()
  for name in datasetList:
    datasetSourcesDict[name] = {
      'dataset': os.path.join(datasetRoot, name),
      'chkPath': os.path.join(chkptRoot, name),
      'pklFilename': '%s.pkl' % name,
      'faultCheckFilename': '%s_faults.xlsx' % name,
      'dataChkptTemplate': 'events_%d.data',
      'dataIndexFile': 'data_index.json',
      'entropyJsonFile': 'entropy.json',
      'eventCheckXlsxFile': '%s.xlsx' % name
    }
  return datasetSourcesDict


def DatasetCheckEvent(tbname, datasetSourcesDict):
  tbMeta = datasetSourcesDict[tbname]
  chkPath = tbMeta['chkPath']
  os.makedirs(chkPath, exist_ok=True)
  xlsxFilename = os.path.join(chkPath, tbMeta['faultCheckFilename'])
  datasetCheckpointFilename = os.path.join(chkPath, tbMeta['pklFilename'])
  dataset = LoadCasasDataset(
    checkpoint=datasetCheckpointFilename,
    datasetPath=tbMeta['dataset']
  )
  result = dataset.check_event_list(xlsxFilename)
  fp = open(os.path.join(chkPath, 'faults.txt'), 'w')
  fp.write(result)
  fp.close()


if __name__ == "__main__":
  parser = argparse.ArgumentParser(
    description="Calculate entropy rate for smart home datasets"
  )
  parser.add_argument('-i', '--info', type=str,
                      help='Site information json file')
  parser.add_argument('-s', '--sites', type=str,
                      help='Sites to download, separated by comma (,)')
  parser.add_argument('-a', '--all', action='store_true',
                      help='Download all datasets')
  parser.add_argument('-P', '--parallel', type=int, default=0,
                      help='Number of parallel threads. '
                           'Default to 0 (use all processors).')
  parser.add_argument('-d', '--dataset-root', type=str,
                      help='Root directory of all datasets')
  parser.add_argument('-o', '--output',
                      help='Folder to save the output files')
  args = parser.parse_args()
  infoFilename = args.info
  outputPath = args.output
  processAll = args.all
  sites = str(args.sites).split(',')
  numProcMax = args.parallel
  datasetRootPath = args.dataset_root

  # Get all sites
  siteInfoDict = LoadSitesSummary(infoFilename)

  if processAll or len(sites) == 0:
    sites = list(siteInfoDict.keys())

  # Make sure output path exists
  os.makedirs(outputPath, exist_ok=True)

  datasetSourcesDict = constructDictDataSources(
    sites, outputPath, datasetRootPath
  )

  # Process array
  if numProcMax <= 0:
    pool = multiprocessing.Pool()
  else:
    pool = multiprocessing.Pool(processes=numProcMax)

  for tbname in sites:
    pool.apply_async(DatasetCheckEvent, (tbname, datasetSourcesDict))

  pool.close()
  pool.join()
