import os
import json
import numpy as np
from mobpredict.entropy import estimate_predictability


def LoadEntropyRateAndProdictability(chkptPath, tbname):
    """Find calculated entropy rate and predictability of a given smart home.
    """
    if not os.path.isdir(chkptPath):
        raise NotADirectoryError('Folder %s not found.' % chkptPath)
    tbChkptPath = os.path.join(chkptPath, tbname)
    if not os.path.isdir(tbChkptPath):
        raise NotADirectoryError(
            'Checkpoint folder %s for smart home %s not found.' % (
                tbChkptPath, tbname
            )
        )
    entropyFilename = os.path.join(tbChkptPath, 'entropy.json')
    if not os.path.isfile(entropyFilename):
        raise FileNotFoundError(
            'Entropy rate and prediction result not found. Make sure the file '
            '%s for smart home %s exists.' % (entropyFilename, tbname)
        )
    # summaryFilename = os.path.join(tbChkptPath, 'summary.json')
    # if not os.path.isfile(summaryFilename):
    #     raise FileNotFoundError(
    #         'Dataset summary file not found. Make sure that the file '
    #         '%s for smart home %s exists' % (summaryFilename, tbname)
    #     )
    fp = open(entropyFilename, 'r')
    dictEntropy = json.load(fp)
    fp.close()
    # fp = open(summaryFilename, 'r')
    # dictSummary = json.load(fp)
    # fp.close()

    # alphabetSize = dictSummary['num_sensors']
    alphabetSize = 20

    for method in dictEntropy:
        entropyRate = dictEntropy[method]['entropy_rate']
        predictability = dictEntropy[method]['predictability']
        length = dictEntropy[method]['length']
        for i, entropy in enumerate(entropyRate):
            predictability[i] = estimate_predictability(
                alphabetSize, entropy
            )
            if not (0 <= predictability[i] <= 1):
                predictability[i] = estimate_predictability(
                    alphabetSize, entropy
                )
                raise ValueError('Error estimating predictability')
            print(predictability[i])
    fp = open(entropyFilename, 'w')
    json.dump(dictEntropy, fp)
    fp.close()


# testbeds = [
#     # Two or more residents
#     'hh107', 'hh121', 'tm004', 'rw104', 'rw110',
#     'atmo1', 'atmo2', 'atmo4', 'atmo6', 'atmo7',
#     'atmo8', 'atmo9', 'atmo10',
#     # Two residents + pets
#     'cairo', 'paris',
#     # Single resident + pets
#     'rw103', 'milan',
#     # Single resident
#     'hh101', 'hh110',
#     'aruba', 'hh102', 'hh103', 'hh104', 'hh105', 'hh106', 'hh108', 'hh109',
#     'hh111', 'hh112', 'hh113', 'hh114', 'hh115', 'hh116', 'hh117', 'hh118',
#     'hh119', 'hh120', 'hh122', 'hh123', 'hh124', 'hh125', 'hh126',
#     'hh127', 'hh128', 'hh129', 'hh130'
# ]
testbeds=['mc1']

chkptPath = 'data'


if __name__ == "__main__":
    result = {}
    for tbname in testbeds:
        print('%s --' % tbname)
        LoadEntropyRateAndProdictability(
            chkptPath=chkptPath, tbname=tbname
        )
