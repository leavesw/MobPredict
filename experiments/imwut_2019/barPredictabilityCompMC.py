"""Scatter plot of predictability and entropy rate of each smart home, against
the number of sensors deployed in those smart homes.
"""
import os
import json
import argparse
import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict


def IsSingle(tbname, datasetNameMapDict):
  """Check if the dataset is recorded in a single-resident smart home.

  The information is provided by the name map. Any dataset starts with 'm' or
  'b' in the mapped name is recorded in a multi-resident smart home.

  Args:
    tbname: str, original name of the dataset
    datasetNameMapDict: dict, containing the name map of the datasets, indexed
      by their original names.

  Returns:
    True, if the dataset is recorded in a single-resident smart home;
    False, otherwise.
  """
  mappedName = datasetNameMapDict[tbname]
  if mappedName[0] == 'b' or mappedName[0] == 'm':
    return False
  else:
    return True


def HasPet(tbname, datasetNamedMapDict):
  """Check if the dataset is recorded in a smart home with pets.

  The information is provided by the name map. Any dataset starts with 'm' or
  'b' in the mapped name is recorded in a multi-resident smart home.

  Args:
    tbname: str, original name of the dataset
    datasetNameMapDict: dict, containing the name map of the datasets, indexed
      by their original names.

  Returns:
    True, if the dataset is recorded in a single-resident smart home;
    False, otherwise.
  """
  mappedName = datasetNamedMapDict[tbname]
  if mappedName[0] == 'b' or mappedName[0] == 'p':
    return True
  else:
    return False


def LoadDatasetNameMap(filename):
  """Load name map from a file.

  Args:
      filename: str, path to the smart home name map

  Returns:
    Ordered dictionary of mapped dataset names.
  """
  datasetNameMapDict = OrderedDict()
  fp = open(filename, 'r')
  for line in fp.readlines():
    line = line.strip()
    if len(line) == 0 or line[0] == '#':
      continue
    tokens = line.split()
    print(tokens)
    datasetNameMapDict[tokens[0]] = tokens[1]
  fp.close()
  return datasetNameMapDict


def LoadSitesSummary(summaryFile):
  """Load CASAS Smart Home site summaries.

  Args:
    summaryFile: Smart home site information JSON file.

  Returns:
    Dictionary of smart home site indexed by site name.
  """
  if os.path.isfile(summaryFile):
    fp = open(summaryFile, 'r')
    siteInfoList = json.load(fp)
    fp.close()
    siteInfoDict = OrderedDict()
    for site in siteInfoList:
      siteInfoDict[site['name']] = site
    return siteInfoDict
  else:
    raise FileNotFoundError(
      'Cannot find summary file for CASAS sites at %s. (Absolute path: %s)' %
      (summaryFile, os.path.abspath(summaryFile))
    )


def LoadEntropyRateAndProdictability(chkptRoot, tbname, method='est'):
  """Find calculated entropy rate and predictability of a given smart home.

  Args:
    chkptRoot: str, path to the root directory of dataset checkpoints
    tbname: str, original name of the dataset
    method: str, methods used to calculate entropy rate ('est')

  Returns:
    dict, containing the information of the predictability of the dataset.
  """
  if not os.path.isdir(chkptRoot):
    raise NotADirectoryError('Folder %s not found.' % chkptRoot)
  tbChkptPath = os.path.join(chkptRoot, tbname)
  if not os.path.isdir(tbChkptPath):
    raise NotADirectoryError(
      'Checkpoint folder %s for smart home %s not found.' % (
        tbChkptPath, tbname
      )
    )
  entropyFilename = os.path.join(tbChkptPath, 'entropy.json')
  if not os.path.isfile(entropyFilename):
    raise FileNotFoundError(
      'Entropy rate and prediction result not found. Make sure the file '
      '%s for smart home %s exists.' % (entropyFilename, tbname)
    )
  summaryFilename = os.path.join(tbChkptPath, 'summary.json')
  if not os.path.isfile(summaryFilename):
    raise FileNotFoundError(
      'Dataset summary file not found. Make sure that the file '
      '%s for smart home %s exists' % (summaryFilename, tbname)
    )
  fp = open(entropyFilename, 'r')
  dictEntropy = json.load(fp)
  fp.close()
  if method not in dictEntropy:
    raise ValueError(
      'Method %s not implemented during the entropy rate estimation of '
      'smart home %s.' % (method, tbname)
    )
  entropyRate = dictEntropy[method]['entropy_rate'][-1]
  predictability = dictEntropy[method]['predictability'][-1]
  length = dictEntropy[method]['length'][-1]
  fp = open(summaryFilename, 'r')
  dictSummary = json.load(fp)
  fp.close()
  return {
    'entropy_rate': entropyRate,
    'predictability': predictability,
    'length': length,
    'num_sensors': dictSummary['num_sensors'],
    'num_weeks': dictSummary['num_weeks'],
    'week_boundaries': dictSummary['week_boundaries']
  }


if __name__ == '__main__':
  parser = argparse.ArgumentParser(
      description='Predictability Scatter Plot'
  )
  parser.add_argument('-i', '--info', type=str,
                      help='Site information json file')
  parser.add_argument('-m', '--map', type=str,
                      help='Dataset map file')
  parser.add_argument('-s', '--sites', type=str, default='',
                      help='Sites to download, separated by comma (,)')
  parser.add_argument('-c', '--chkpt-root', type=str,
                      help='Root directory of checkpoints')
  parser.add_argument('-o', '--output',
                      help='Directory to store scatter plot files.')
  args = parser.parse_args()
  infoFilename = args.info
  mapFilename = args.map
  outputPath = args.output
  sites = str(args.sites)
  chkptRoot = args.chkpt_root

  # Get all sites
  siteInfoDict = LoadSitesSummary(infoFilename)
  datasetNameMapDict = LoadDatasetNameMap(mapFilename)

  # Sites to plot
  if len(sites) == 0:
    sites = list(siteInfoDict.keys())
  else:
    sites = sites.split(',')

  #
  result = {}
  mcResult = {}
  for tbname in sites:
    try:
      result[tbname] = LoadEntropyRateAndProdictability(
        chkptRoot=chkptRoot, tbname=tbname
      )
      mcResult[tbname] = LoadEntropyRateAndProdictability(
        chkptRoot=chkptRoot, tbname=tbname, method='mc'
      )
    except Exception as error:
      print("Unexpected Error: ", error)
      if tbname in result:
        del result[tbname]
  # Capture the result
  data = OrderedDict(
    single_resident={
      'label': 'Single Resident\nSmart Homes',
      'mc_predictability': [],
      'predictability': [],
      'entropy_rate': []
    }, multi_resident={
      'label': 'Multi-resident\nSmart Homes',
      'mc_predictability': [],
      'predictability': [],
      'entropy_rate': []
    }, sr_with_pets={
      'label': 'Single Resident\nwith Pets',
      'mc_predictability': [],
      'predictability': [],
      'entropy_rate': []
    }, mr_with_pets={
      'label': 'Multi-resident\nwith Pets',
      'mc_predictability': [],
      'predictability': [],
      'entropy_rate': []
    }
  )
  for tbname in result:
    if tbname not in datasetNameMapDict:
      continue
    tbIsSR = IsSingle(tbname, datasetNameMapDict)
    tbHasPets = HasPet(tbname, datasetNameMapDict)
    targets = []
    if tbIsSR:
      targets.append(data['single_resident'])
      if tbHasPets:
        targets.append(data['sr_with_pets'])
    else:
      targets.append(data['multi_resident'])
      if tbHasPets:
        targets.append(data['mr_with_pets'])
    for target in targets:
      target['predictability'].append(
        result[tbname]['predictability']
      )
      target['mc_predictability'].append(
        mcResult[tbname]['predictability']
      )
      target['entropy_rate'].append(
        result[tbname]['entropy_rate']
      )
  # Building Statistics
  stats = OrderedDict()
  for label in data:
    stats[label] = {
      'predictability': {
        'mean': np.mean(data[label]['predictability']),
        'std': np.std(data[label]['predictability']),
        'min': np.min(data[label]['predictability']),
        'max': np.max(data[label]['predictability'])
      },
      'mc_predictability': {
        'mean': np.mean(data[label]['mc_predictability']),
        'std': np.std(data[label]['mc_predictability']),
        'min': np.min(data[label]['mc_predictability']),
        'max': np.max(data[label]['mc_predictability'])
      },
      'entropy_rate': {
        'mean': np.mean(data[label]['entropy_rate']),
        'std': np.std(data[label]['entropy_rate']),
        'min': np.min(data[label]['entropy_rate']),
        'max': np.max(data[label]['entropy_rate'])
      }
    }

  # Plot the statistics
  prop_cycle = plt.rcParams['axes.prop_cycle']
  colors = prop_cycle.by_key()['color']
  barWidth = 0.35
  # Set figures
  fig, ax1 = plt.subplots(
    nrows=1, ncols=1, figsize=(7, 7)
  )
  xlabels = [
    data[label]['label'] for label in stats
  ]
  xticks = np.arange(len(xlabels)) + 1
  predMean = [
    stats[label]['predictability']['mean'] for label in stats
  ]
  predStd = [
    stats[label]['predictability']['std'] for label in stats
  ]
  mcPredMean = [
    stats[label]['mc_predictability']['mean'] for label in stats
  ]
  mcPredStd = [
    stats[label]['mc_predictability']['std'] for label in stats
  ]
  ax1.bar(
    xticks,
    predMean,
    width=barWidth,
    yerr=predStd,
    align='center',
    alpha=0.5,
    capsize=10,
    ecolor='k',
    label='Estimated Predictability'
  )
  ax1.bar(
    xticks + barWidth,
    mcPredMean,
    width=barWidth,
    yerr=mcPredStd,
    align='center',
    alpha=0.5,
    capsize=10,
    ecolor='k',
    label='Markov Chain'
  )
  ax1.set_ylim(0, 1)
  ax1.set_xticks(xticks + barWidth / 2)
  ax1.set_xticklabels(xlabels)
  ax1.set_ylabel('Predictability')
  ax1.yaxis.grid(True)
  ax1.legend()
  plt.tight_layout()
  plt.savefig(
    os.path.join(os.path.dirname(__file__), 'barPredictabilityCompMC.pdf')
  )
  print(json.dumps(stats, indent=2))
