"""This is a script that finds all testbeds available in the data folder
and create a DataSource structure that you can copy and past to other scripts.
"""
import os
import sys


testbeds = [
    # Two or more residents
    'hh107', 'hh121', 'tm004', 'kyoto', 'tulum', 'rw104', 'rw110',
    'atmo1', 'atmo2', 'atmo4', 'atmo5', 'atmo6', 'atmo7',
    'atmo8', 'atmo9', 'atmo10',
    # Two residents + pets
    'cairo', 'paris',
    # Single resident + pets
    'rw103', 'milan',
    # Single resident
    'hh101', 'hh110',
    'aruba', 'hh102', 'hh103', 'hh104', 'hh105', 'hh106', 'hh108', 'hh109',
    'hh111', 'hh112', 'hh113', 'hh114', 'hh115', 'hh116', 'hh117', 'hh118',
    'hh119', 'hh120', 'hh122', 'hh123', 'hh124', 'hh125', 'hh126',
    'hh127', 'hh128', 'hh129', 'hh130'
]

datasetSearchPath = 'data/casas/data'
siteSearchPath = 'data/casas/site'

if __name__ == '__main__':
    availableDatasets = os.listdir(datasetSearchPath)
    availableSites = os.listdir(siteSearchPath)
    availableSitesCanonical = [
        tbname.lower() for tbname in availableSites
    ]
    availableDatasetsCanonical = [
        dsname.lower() for dsname in availableDatasets
    ]
    for tbname in testbeds:
        try:
            siteId = availableSitesCanonical.index(tbname.lower())
        except ValueError:
            print(
                'Site %s not found.' % tbname, file=sys.stderr
            )
            continue
        datasetFilename = None
        for dsId, dsname in enumerate(availableDatasetsCanonical):
            if tbname.lower() in dsname:
                datasetFilename = availableDatasets[dsId]
                break
        if datasetFilename is None:
            print(
                'Dataset associated with site %s is not found.' % tbname,
                file=sys.stderr
            )
        else:
            print('\'%s\': {' % tbname)
            print('\'dataset\': \'%s\',' % (
                  datasetSearchPath + '/' + datasetFilename
                  ))
            print('\'site\': \'%s\',' % (
                  siteSearchPath + '/' + availableSites[siteId]
                  ))
            print('\'chkPath\': \'data/chkpt/%s\',' % tbname)
            print('\'pklFilename\': \'%s.pkl\',' % tbname)
            print('\'dataChkptTemplate\': \'events_%d.data\',')
            print('\'dataIndexFile\': \'data_index.json\',')
            print('\'entropyJsonFile\': \'entropy.json\'')
            print('},')
