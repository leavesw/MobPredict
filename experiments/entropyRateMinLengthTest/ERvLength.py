"""This experiments investigate the relationship between the lengthArray of the
observation and the estimated entropy rate and predictability.

In the experiment, we select three sites, each with long recorded sensor events,
to test the relationship between the estimation accuracy of entropy rate and
the lengthArray of the observation.

The experiments include the following smart home sites:
- TM004, 213 days, 2-3 residents, 24 sensors.
- HH107, 370 days, 2+ residents, 24 sensors (dropped).
- hh108, 2164 days, 1 resident, 28 sensors
- hh117, 352 days, 1 resident, 12 sensors
"""
import os
import json
import argparse
import multiprocessing
import numpy as np
from collections import OrderedDict
import matplotlib.pyplot as plt
from pycasas.data import CASASDataset
from mobpredict.compression import PPM
from mobpredict.compression import LZ77
from mobpredict.compression import LZMA
from mobpredict.compression import Archive
from mobpredict.entropy import entropy_actual
from mobpredict.entropy import entropy_rand
from mobpredict.entropy import entropy_uncorrelated
from mobpredict.entropy import entropy_est
from mobpredict.entropy import entropyEstimator
from mobpredict.entropy import entropyMC
from mobpredict.entropy import estimate_predictability


here = os.path.dirname(__file__)


def LoadSitesSummary(summaryFile):
  """Load CASAS Smart Home site summaries.

  Args:
    summaryFile: Smart home site information JSON file.

  Returns:
    Dictionary of smart home site indexed by site name.
  """
  if os.path.isfile(summaryFile):
    fp = open(summaryFile, 'r')
    siteInfoList = json.load(fp)
    fp.close()
    siteInfoDict = OrderedDict()
    for site in siteInfoList:
      siteInfoDict[site['name']] = site
    return siteInfoDict
  else:
    raise FileNotFoundError(
      'Cannot find summary file for CASAS sites at %s. (Absolute path: %s)' %
      (summaryFile, os.path.abspath(summaryFile))
    )


def GenerateEventsDataFile(filename, length, sensorSequence):
  """Generate sensor sequence data file from sensor sequence

  Args:
      filename (str): Filename to store simulated trial observation.
      length (int, optional): Defaults to 1000. Length of the simulated
          observation.
      sensorSequence (:obj:`numpy.ndarray`): Sensor sequence.
  """
  fp = open(filename, 'wb')
  # Instead of using np.random.choice, np.random.multinomial is more than 50%
  # faster.
  sequence = np.array(sensorSequence[:length], dtype=np.uint8)
  fp = open(filename, 'wb')
  fp.write(bytes(sequence))
  fp.close()
  return sequence


def EstimateEntropyRate(dataFile, method):
  """Estimate the entropy rate based on observation stored in dataFile.

  Args:
      dataFile (str): Filename that stores the observation sequence.
      method (str): The method used to estimate the entropy rate. Possible
          values include 'ppm', 'lz77', 'lzma', 'random', 'uncorrelated', or
          'actual'.
  """
  if not os.path.isfile(dataFile):
    raise FileNotFoundError(
      'Data file %s not found. Absolute Path: %s.' % (
        dataFile, os.path.abspath(dataFile)
      )
    )
  if method in ['ppm', 'lz77', 'lzma']:
    # These are compression based method
    dictCompressors = {
      'ppm': PPM(),
      'lz77': LZ77(),
      'lzma': LZMA()
    }
    dataFileDir = os.path.dirname(dataFile)
    dataFileNoExt = os.path.splitext(os.path.basename(dataFile))[0]
    outFilename = os.path.join(
      dataFileDir, '%s_%s.7z' % (dataFileNoExt, method)
    )
    # Compress only if the compressed file does not exist.
    if not os.path.isfile(outFilename):
      dictCompressors[method].compress(dataFile, outFilename)
    # Compute compression-based entropy rate
    archive = Archive(outFilename)
    return archive.bitRate
  else:
    fp = open(dataFile, 'rb')
    data = fp.read()
    data = bytes(data)
    fp.close()
    if method == 'random':
      reachableSymbols = []
      for s in data:
        if s not in reachableSymbols:
          reachableSymbols.append(s)
      return entropy_rand(reachableSymbols)
    elif method == 'uncorrelated':
      return entropy_uncorrelated(data)
    elif method == 'actual':
      return entropy_actual(data, max_length=100)
    elif method == 'est':
      return entropy_est(data)


def LoadDataIndex(dataIndexFile):
  """Load data index dictionary from file.

  Args:
      dataIndexFile (str): Path to the data index JSON file.

  Returns:

  """
  if os.path.isfile(dataIndexFile):
    fp = open(dataIndexFile, 'r')
    dictData = json.load(fp)
    fp.close()
  else:
    dictData = {}
  return dictData


def SaveDataIndex(tag, length, filename, dataIndexFile):
  """Save structure of checkpoint files

  Args:
      tag (str): Data tag. `origin`, `lz77`, `lzma`, or `ppm`.
      length (int): Length of the data ponts.
      filename (str): Name of the checkpoint file.
  """
  dictData = LoadDataIndex(dataIndexFile)
  if tag not in dictData:
    dictData[tag] = {
      'length': [],
      'filename': []
    }
  dictData[tag]['length'].append(int(length))
  dictData[tag]['filename'].append(os.path.basename(filename))
  fp = open(dataIndexFile, 'w')
  json.dump(dictData, fp, indent=4)
  fp.close()
  return dictData


def GetCheckpointFileFromDataIndex(tag, length, dictData):
  """Get the checkpoint file identified by $tag and $lengthArray exists in the
  data index dictionary.

  Args:
    tag (str): Data tag, such as 'origin' and so on.
    length (int): Length of observation.
    dictData (dict): Data index dictonary.

  Returns:
    str, None: Returns the checkpoint filename, or None, if such checkpoint
      file does not exist.
  """
  if tag in dictData and length in dictData[tag]['length']:
    index = dictData[tag]['length'].index(length)
    return dictData[tag]['filename'][index]
  else:
    return None


def LoadEntropyRate(entropyRateFile):
  """Load entropy rate structure from JSON file.

  Args:
    entropyRateFile (str): The JSON file that stored the entropy rate.
  """
  if os.path.isfile(entropyRateFile):
    fp = open(entropyRateFile, 'r')
    dictEntropyRate = json.load(fp)
    fp.close()
  else:
    dictEntropyRate = {}
  return dictEntropyRate


def SaveEntropyRate(tag, length, entropyRate, predictability, entropyRateFile):
  """Save entropy rate to JSON file.

  Args:
    tag (str): Name of the method used to estimate entropy rate.
    length (int): Length of the observation.
    entropyRate (float): Estimated entropy rate.
    predictability (float): Upper bound of predictability based on Fano's
      inequality.
    entropyRateFile (str): The JSON file to store the entropy rates.
  """
  dictEntropy = LoadEntropyRate(entropyRateFile)
  if tag not in dictEntropy:
    dictEntropy[tag] = {
      'length': [],
      'entropy_rate': [],
      'predictability': []
    }
  dictEntropy[tag]['length'].append(length)
  dictEntropy[tag]['entropy_rate'].append(entropyRate)
  dictEntropy[tag]['predictability'].append(predictability)
  fp = open(entropyRateFile, 'w')
  json.dump(dictEntropy, fp, indent=4, sort_keys=True)
  fp.close()
  return dictEntropy


def GetEntropyRatePredictability(tag, length, dictEntropyRate):
  """Load entropy rate and predictability from saved JSON dictionary.

  Args:
      tag (str): Name of the method used to estimate entropy rate.
      length (int): Length of the observation.
      entropyRateFile (str): The JSON file to store the entropy rates.
  """
  if tag in dictEntropyRate and length in dictEntropyRate[tag]['length']:
    entry = dictEntropyRate[tag]
    index = entry['length'].index(length)
    return {
      'entropy_rate': entry['entropy_rate'][index],
      'predictability': entry['predictability'][index]
    }
  else:
    return None


def PlotEntropyRate(dictEntropyRate,
                    tbname,
                    ref=None,
                    weekRef=None,
                    methodLabels=None,
                    filename=None):
  """Plot estimated entropy rate and predictability

  Args:
    dictEntropyRate (dict): Dictionary containing calculated entropy rate
      and predictability.
    ref (dict): The true entropy rate and predictability.
  """
  if methodLabels is None:
    methodLabels = {
      m: m for m in dictEntropyRate
    }
  fig, [ax1, ax2] = plt.subplots(
    nrows=1, ncols=2, figsize=(20, 8)
  )
  if ref is not None:
    ax1.axhline(y=ref['entropy_rate'], color='r')
    ax2.axhline(y=ref['predictability'], color='r')
  if weekRef is not None:
    for week in weekRef:
      ax1.axvline(x=weekRef[week]['index'], color='r')
      ax2.axvline(x=weekRef[week]['index'], color='r')
  for m in dictEntropyRate.keys():
    ax1.plot(
      dictEntropyRate[m]['length'],
      dictEntropyRate[m]['entropy_rate'],
      label=methodLabels[m]
    )
    # When plotting the predictability, we need to consider the cases
    # where there is not enough observation for entropy rate to be properly
    # estimated.
    ax2ValidIndices = [
      i for i in range(len(dictEntropyRate[m]['length']))
      if dictEntropyRate[m]['predictability'][i] is not None
    ]
    validLength = [
      dictEntropyRate[m]['length'][i] for i in ax2ValidIndices
    ]
    validPredictability = [
      dictEntropyRate[m]['predictability'][i] for i in ax2ValidIndices
    ]
    ax2.plot(
      validLength, validPredictability, label=methodLabels[m]
    )
  ax1.set_xscale('log')
  ax1.set_yscale('log')
  ax1.set_xlabel('Length of the observation.')
  ax1.set_ylabel('Estimated entropy rate (bits)')
  ax1.grid(
    axis='both',
    which='major',
    linestyle='--',
    linewidth='0.5',
    color='gray'
  )
  ax1.grid(
    axis='both',
    which='minor',
    linestyle=':',
    linewidth='0.5',
    color='gray'
  )
  ax1.set_title('Estimated Entropy Rate')
  ax1.legend()
  ax2.set_xscale('log')
  ax2.set_xlabel('Length of the sequence')
  ax2.set_ylabel('Estimated entropy rate (bits)')
  ax2.grid(
    axis='both',
    which='major',
    linestyle='--',
    linewidth='0.5',
    color='gray'
  )
  ax2.grid(
    axis='both',
    which='minor',
    linestyle=':',
    linewidth='0.5',
    color='gray'
  )
  ax2.set_title(
    'Predictability'
  )
  ax2.legend()
  if weekRef is not None:
    ax1Lim = ax1.get_ylim()
    ax2Lim = ax2.get_ylim()
    for week in weekRef:
      ax1.text(weekRef[week]['index'], ax1Lim[1], weekRef[week]['label'],
               rotation=90, horizontalalignment='right',
               verticalalignment='top')
      ax2.text(weekRef[week]['index'], ax2Lim[1], weekRef[week]['label'],
               rotation=90, horizontalalignment='right',
               verticalalignment='top')
  if filename is None:
    plt.show()
  else:
    plt.savefig(filename)


def LoadCasasDataset(checkpoint, datasetPath):
  """Load CASAS dataset from checkpoints or folder containing original data

  Args:
      checkpoint: str, path to dataset checkpoint
  """
  if os.path.isfile(checkpoint):
    dataset = CASASDataset.load(checkpoint)
  else:
    dataset = CASASDataset(datasetPath)
    dataset.enable_sensor_with_sensor_categories(
      ['Motion', 'Door', 'Item']
    )
    dataset.load_events(True)
    dataset.save(checkpoint)
  return dataset


def FindWeekBoundary(timeList):
  """Find indices on week boundary.
  Args:
    timeList: List of time tag corresponding to each sensor event.

  Returns:
    List of indices at the weekly boundary
  """
  week_index_list = [0]
  start_date = timeList[0].date()
  for i in range(len(timeList)):
    cur_date = timeList[i].date()
    # Monday - then not the same day as start_date
    # Else, if more than 7 days apart
    if (cur_date.weekday() == 0 and cur_date > start_date) or (
        cur_date - start_date).days >= 7:
      week_index_list.append(i)
      start_date = cur_date
  week_index_list.append(len(timeList))
  return week_index_list


def SaveDatasetSummary(filename, dataset, timeSequence):
  """Save summary of dataset to json file.

  Args:
    filename:
    dataset:
    timeSequence:

  Returns:
  """
  assert (isinstance(dataset, CASASDataset))
  startTime = timeSequence[0]
  endTime = timeSequence[-1]
  weekBoundaries = FindWeekBoundary(timeSequence)
  summary = {
    'name': dataset.get_name(),
    'startDate': startTime.strftime("%m/%d/%Y %H:%M:%S.%f ") +
                 startTime.strftime("%z")[:3] + ":" +
                 startTime.strftime("%z")[3:],
    'stopDate': endTime.strftime("%m/%d/%Y %H:%M:%S.%f ") +
                endTime.strftime("%z")[:3] + ":" +
                endTime.strftime("%z")[3:],
    'num_sensors': len(dataset.sensor_list),
    'num_weeks': len(weekBoundaries) - 1,
    'week_boundaries': weekBoundaries
  }
  fp = open(filename, 'w')
  json.dump(summary, fp, indent=4)
  fp.close()


def constructDictDataSources(datasetList,
                             chkptRoot,
                             datasetRoot):
  """Construct a dictionary containing file names for checkpoints, dataset
  directories, etc.

  Args:
    datasetList: list of dataset names
    chkptRoot: path of the root directory for checkpoint files
    datasetRoot: path of the root directory where the dataset are stored

  Returns:
    A dictionary including metadata for dataset files, indexed by dataset name.
  """
  datasetSourcesDict = OrderedDict()
  for name in datasetList:
    datasetSourcesDict[name] = {
      'dataset': os.path.join(datasetRoot, name),
      'chkPath': os.path.join(chkptRoot, name),
      'pklFilename': '%s.pkl' % name,
      'dataChkptTemplate': 'events_%d.data',
      'dataIndexFile': 'data_index.json',
      'entropyJsonFile': 'entropy.json',
      'eventCheckXlsxFile': '%s.xlsx' % name
    }
  return datasetSourcesDict


def computeDataset(tbname, datasetSourcesDict):
  """Entropy rate computation routine of each dataset.
  """
  tbMeta = datasetSourcesDict[tbname]
  # Make sure that the directory to store checkpoint data exists.
  chkPath = tbMeta['chkPath']
  os.makedirs(chkPath, exist_ok=True)
  dataIndexFilename = os.path.join(chkPath, tbMeta['dataIndexFile'])
  datasetCheckpointFilename = os.path.join(chkPath, tbMeta['pklFilename'])
  entropyRateFilename = os.path.join(chkPath, tbMeta['entropyJsonFile'])
  datasetSummaryFilename = os.path.join(chkPath, 'summary.json')
  dictData = LoadDataIndex(dataIndexFilename)
  dataset = LoadCasasDataset(
    checkpoint=datasetCheckpointFilename,
    datasetPath=tbMeta['dataset']
  )
  sensor_seq, time_seq = dataset.to_sensor_sequence()
  totalLength = len(sensor_seq)
  print('Total sensor events: %d' % totalLength)
  if True:  # not os.path.isfile(datasetSummaryFilename):
    SaveDatasetSummary(datasetSummaryFilename, dataset, time_seq)
  # Estimate entropy rate at various lengthArray. The lengthArray is spaced in
  # log scale. However, we also want to add time boundaries in by week.
  lengthArray = [int(l) for l in np.round(
    np.logspace(1, np.log10(totalLength), 20)
  ).astype(np.int)] + FindWeekBoundary(time_seq)[1:]
  # Remove duplicated values
  lengthArray = list(set(lengthArray))
  lengthArray.sort()
  for l in lengthArray:
    print(
      'Generating observation of lengthArray %d for compression-based'
      ' entropy rate estimator.' % l
    )
    if GetCheckpointFileFromDataIndex(
      tag='origin',
      length=l,
      dictData=dictData
    ) is None:
      filename = tbMeta['dataChkptTemplate'] % l
      filename = os.path.join(chkPath, filename)
      if not os.path.isfile(filename):
        GenerateEventsDataFile(
          filename=filename,
          length=l,
          sensorSequence=sensor_seq
        )
      dictData = SaveDataIndex(
        tag='origin',
        length=l,
        filename=filename,
        dataIndexFile=dataIndexFilename
      )
  # Secondly, estimate entropy rate
  dictEntropy = LoadEntropyRate(entropyRateFilename)
  for m in estimationMethods:
    print(
      'Computing entropy rate and predictability with method %s.' % m
    )
    if m == 'est' or m == 'mc':
      # Entropy rate estimation can be accelerated by providing
      # Length array. To check if we have this saved, check for the
      # longest distance.
      l = lengthArray[-1]
      if GetEntropyRatePredictability(m, l, dictEntropy) is None:
        dataFile = os.path.join(
          chkPath,
          GetCheckpointFileFromDataIndex(
            tag='origin',
            length=l,
            dictData=dictData
          )
        )
        if not os.path.isfile(dataFile):
          raise FileNotFoundError(
            'Data file %s not found. Absolute Path: %s.' % (
              dataFile, os.path.abspath(dataFile)
            )
          )
        fp = open(dataFile, 'rb')
        data = fp.read()
        data = bytes(data)
        fp.close()
        if m == 'est':
          entropyRates = entropyEstimator(data, lengthArray)
        elif m == 'mc':
          entropyRates = entropyMC(
            data, dataset.sensor_list, lengthArray
          )
        for l_id, l in enumerate(lengthArray):
          dictEntropy = SaveEntropyRate(
            tag=m,
            length=l,
            entropyRate=entropyRates[l_id],
            predictability=estimate_predictability(
              alphabetSize=len(dataset.sensor_list),
              entropy=entropyRates[l_id]
            ),
            entropyRateFile=entropyRateFilename
          )
    else:
      for l in lengthArray:
        if GetEntropyRatePredictability(m, l, dictEntropy) is None:
          entropyRate = EstimateEntropyRate(
            dataFile=os.path.join(
              chkPath,
              GetCheckpointFileFromDataIndex(
                tag='origin',
                length=l,
                dictData=dictData
              )
            ),
            method=m
          )
          predictability = estimate_predictability(
            alphabetSize=len(dataset.sensor_list),
            entropy=entropyRate
          )
          dictEntropy = SaveEntropyRate(
            tag=m,
            length=l,
            entropyRate=entropyRate,
            predictability=predictability,
            entropyRateFile=entropyRateFilename
          )
  # Finally plot the result
  # Build week reference first
  weekList = FindWeekBoundary(time_seq)
  weekRef = {}
  for week in weekGuidelines:
    if weekGuidelines[week] >= len(weekList) - 1:
      break
    weekRef[week] = {
      'label': week,
      'index': weekList[weekGuidelines[week] + 1]
    }
  PlotEntropyRate(
    dictEntropyRate=dictEntropy,
    weekRef=weekRef,
    methodLabels=estimationMethods,
    tbname=tbname,
    filename=os.path.join(chkPath, '%s_predictability.pdf' % tbname)
  )


estimationMethods = {
  'ppm': 'PPM Compression',
  'lz77': 'LZ77 Compression',
  'lzma': 'LZMA Compression',
  'random': r'$S^{rand}$',
  'uncorrelated': r'$S^{unc}$',
  'mc': r'$S^{MC}$',
  'est': r'$S^{est}$'
}

weekGuidelines = {
  '1w': 1,
  '2w': 2,
  '3w': 3,
  '4w': 4,
  '2m': 10,
  '3m': 15,
  '6m': 26,
  '1yr': 52,
  '1.5yr': 78,
  '2yr': 104
}

if __name__ == "__main__":
  parser = argparse.ArgumentParser(
    description="Calculate entropy rate for smart home datasets"
  )
  parser.add_argument('-i', '--info', type=str,
                      help='Site information json file')
  parser.add_argument('-s', '--sites', type=str,
                      help='Sites to download, separated by comma (,)')
  parser.add_argument('-a', '--all', action='store_true',
                      help='Download all datasets')
  parser.add_argument('-P', '--parallel', type=int, default=0,
                      help='Number of parallel threads. '
                           'Default to 0 (use all processors).')
  parser.add_argument('-d', '--dataset-root', type=str,
                      help='Root directory of all datasets')
  parser.add_argument('-o', '--output',
                      help='Folder to save the output files')
  args = parser.parse_args()
  infoFilename = args.info
  outputPath = args.output
  processAll = args.all
  sites = str(args.sites).split(',')
  numProcMax = args.parallel
  datasetRootPath = args.dataset_root

  # Get all sites
  siteInfoDict = LoadSitesSummary(infoFilename)

  if processAll or len(sites) == 0:
    sites = list(siteInfoDict.keys())

  # Make sure output path exists
  os.makedirs(outputPath, exist_ok=True)

  datasetSourcesDict = constructDictDataSources(
    sites, outputPath, datasetRootPath
  )

  # Process array
  if numProcMax <= 0:
    pool = multiprocessing.Pool()
  else:
    pool = multiprocessing.Pool(processes=numProcMax)

  for tbname in sites:
    pool.apply_async(computeDataset, (tbname, datasetSourcesDict))

  pool.close()
  pool.join()