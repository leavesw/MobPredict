import os
import json
import numpy as np


def LoadEntropyRateAndProdictability(chkptPath, tbname, method='est'):
    """Find calculated entropy rate and predictability of a given smart home.
    """
    if not os.path.isdir(chkptPath):
        raise NotADirectoryError('Folder %s not found.' % chkptPath)
    tbChkptPath = os.path.join(chkptPath, tbname)
    if not os.path.isdir(tbChkptPath):
        raise NotADirectoryError(
            'Checkpoint folder %s for smart home %s not found.' % (
                tbChkptPath, tbname
            )
        )
    entropyFilename = os.path.join(tbChkptPath, 'entropy.json')
    if not os.path.isfile(entropyFilename):
        raise FileNotFoundError(
            'Entropy rate and prediction result not found. Make sure the file '
            '%s for smart home %s exists.' % (entropyFilename, tbname)
        )
    summaryFilename = os.path.join(tbChkptPath, 'summary.json')
    if not os.path.isfile(summaryFilename):
        raise FileNotFoundError(
            'Dataset summary file not found. Make sure that the file '
            '%s for smart home %s exists' % (summaryFilename, tbname)
        )
    fp = open(entropyFilename, 'r')
    dictEntropy = json.load(fp)
    fp.close()
    if method not in dictEntropy:
        raise ValueError(
            'Method %s not implemented during the entropy rate estimation of '
            'smart home %s.' % (method, tbname)
        )
    entropyRate = dictEntropy[method]['entropy_rate']
    predictability = dictEntropy[method]['predictability']
    length = dictEntropy[method]['lengthArray']
    fp = open(summaryFilename, 'r')
    dictSummary = json.load(fp)
    fp.close()
    return {
        'entropy_rate': entropyRate,
        'predictability': predictability,
        'lengthArray': length,
        'num_sensors': dictSummary['num_sensors'],
        'num_weeks': dictSummary['num_weeks']
    }


testbeds = [
    # Two or more residents
    'hh107', 'hh121', 'tm004', 'rw104', 'rw110',
    # Two residents + pets
    'cairo', 'paris',
    # Single resident + pets
    'rw103', 'milan',
    # Single resident
    'hh101', 'hh110',
    'aruba', 'hh102', 'hh103', 'hh104', 'hh105', 'hh106', 'hh108', 'hh109',
    'hh111', 'hh112', 'hh113', 'hh114', 'hh115', 'hh116', 'hh117', 'hh118',
    'hh119', 'hh120', 'hh122', 'hh123', 'hh124', 'hh125', 'hh126',
    'hh127', 'hh128', 'hh129', 'hh130'
]


chkptPath = 'data/chkpt'


from scipy.optimize import bisect


def estimate_predictability(alphabetSize, entropy, precision=0.01):
    """Determine the upper-bound of predictability based on Fano's inequality.

    The Fano's inequality states that:
    $- P(e) \log_2 P(e) - (1 - P(e)) \log_2 (1 - P(e)) + P(e) \log_2 (N-1)
    \geq H(\mathcal{X})$

    Args:
        alphabetSize (int): Alphabet Size (N)
        entropy (float): Estimated entropy rate of the underlying stochastic
            process.
        precision (float, optional): Percentage precision of the predictability.
    """
    # Calculate lower bound of channel error, $P(e)$, by dynamic programming.
    if entropy >= np.log2(alphabetSize):
        # If entropy is greater than the upperbound of error, which means the
        # estimated entropy is inaccurate. Return None.
        return None

    def FanoFunction(e):
        return e * np.log2(alphabetSize - 1) - \
               e * np.log2(e) - (1 - e) * np.log2(1 - e) - entropy

    # # Initialization
    # e_prev = 0.9
    # e = 0.01
    # while np.abs(e - e_prev) / e_prev > precision:
    #     e_prev = e
    #     e = 1 / np.log2(alphabetSize - 1) * (
    #             entropy + e * np.log2(e) + (1 - e) * np.log2(1 - e)
    #     )
    pE, info = bisect(FanoFunction, 0.01, 0.99, full_output=True)
    # print(FanoFunction(pE))
    if not (0 < pE < 1):
        raise ValueError(
            'Predictability calculation does not converge. '
            'Entropy rate %.4f, alphabetSize %d' % (
                entropy, alphabetSize
            )
        )
    return 1 - pE


if __name__ == "__main__":
    result = {}
    for tbname in testbeds:
        print('%s --' % tbname)
        result[tbname] = LoadEntropyRateAndProdictability(
            chkptPath=chkptPath, tbname=tbname
        )
        alphabetSize = result[tbname]['num_sensors']
        entropyRates = result[tbname]['entropy_rate']
        predictArray = result[tbname]['predictability']
        for i, entropy in enumerate(entropyRates):
            if entropy < np.log2(alphabetSize):
                predictability = estimate_predictability(
                    alphabetSize, entropy, precision=0.01
                )
                if not (1 > predictArray[i] > 0):
                    print("%.5f, %.4f" % (
                        predictArray[i] - predictability, predictability
                    ))
                if not (1 > predictability > 0):
                    print('Error estimating predictability')
                    predictability = estimate_predictability(
                        alphabetSize, entropy, precision=0.01
                    )
