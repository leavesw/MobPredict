"""Scatter plot of predictability and entropy rate of each smart home, against
the number of sensors deployed in those smart homes.
"""
import os
import json
import numpy as np
import matplotlib.pyplot as plt


def IsSingleResident(tbname):
    """Returns true if there is only one resident staying in the smart home.
    """
    return tbname not in {'hh107', 'hh121', 'tm004', 'kyoto', 'tulum', 'rw104',
                          'rw110', 'cairo', 'paris',
                          'atmo1', 'atmo2', 'atmo4', 'atmo5', 'atmo6', 'atmo7',
                          'atmo8', 'atmo9', 'atmo10'}


def HasPets(tbname):
    """Returns true if the smart home has pets.
    """
    return tbname in {'cairo', 'paris', 'rw103', 'milan'}


def LoadEntropyRateAndProdictability(chkptPath, tbname, method='est'):
    """Find calculated entropy rate and predictability of a given smart home.
    """
    if not os.path.isdir(chkptPath):
        raise NotADirectoryError('Folder %s not found.' % chkptPath)
    tbChkptPath = os.path.join(chkptPath, tbname)
    if not os.path.isdir(tbChkptPath):
        raise NotADirectoryError(
            'Checkpoint folder %s for smart home %s not found.' % (
                tbChkptPath, tbname
            )
        )
    entropyFilename = os.path.join(tbChkptPath, 'entropy.json')
    if not os.path.isfile(entropyFilename):
        raise FileNotFoundError(
            'Entropy rate and prediction result not found. Make sure the file '
            '%s for smart home %s exists.' % (entropyFilename, tbname)
        )
    summaryFilename = os.path.join(tbChkptPath, 'summary.json')
    if not os.path.isfile(summaryFilename):
        raise FileNotFoundError(
            'Dataset summary file not found. Make sure that the file '
            '%s for smart home %s exists' % (summaryFilename, tbname)
        )
    fp = open(entropyFilename, 'r')
    dictEntropy = json.load(fp)
    fp.close()
    if method not in dictEntropy:
        raise ValueError(
            'Method %s not implemented during the entropy rate estimation of '
            'smart home %s.' % (method, tbname)
        )
    entropyRate = dictEntropy[method]['entropy_rate']
    predictability = dictEntropy[method]['predictability']
    length = dictEntropy[method]['length']
    fp = open(summaryFilename, 'r')
    dictSummary = json.load(fp)
    fp.close()
    return {
        'entropy_rate': entropyRate,
        'predictability': predictability,
        'length': length,
        'num_sensors': dictSummary['num_sensors'],
        'num_weeks': dictSummary['num_weeks'],
        'week_boundaries': dictSummary['week_boundaries']
    }


testbeds = [
    # Two or more residents
    'hh107', 'hh121', 'tm004', 'rw110',
    'atmo1', 'atmo2', 'atmo4', 'atmo6', 'atmo7',
    'atmo8', 'atmo9', 'atmo10',
    # Two residents + pets
    'cairo', 'paris',
    # Single resident + pets
    'rw103', 'milan',
    # Single resident
    'hh101', 'hh110',
    'aruba', 'hh102', 'hh103', 'hh104', 'hh105', 'hh106', 'hh108', 'hh109',
    'hh111', 'hh112', 'hh113', 'hh114', 'hh115', 'hh116', 'hh117', 'hh118',
    'hh119', 'hh120', 'hh122', 'hh123', 'hh124', 'hh125', 'hh126',
    'hh127', 'hh128', 'hh129', 'hh130'
]

chkptPath = 'data/chkpt'

if __name__ == '__main__':
    result = {}
    for tbname in testbeds:
        result[tbname] = LoadEntropyRateAndProdictability(
            chkptPath=chkptPath, tbname=tbname
        )
    # Capture the result
    data = {
        'single_resident': [],
        'multi_resident': [],
        'sr_with_pets': [],
        'mr_with_pets': []
    }
    for tbname in testbeds:
        tbIsSR = IsSingleResident(tbname)
        tbHasPets = HasPets(tbname)
        targets = []
        if tbIsSR:
            targets.append(data['single_resident'])
            if tbHasPets:
                targets.append(data['sr_with_pets'])
        else:
            targets.append(data['multi_resident'])
            if tbHasPets:
                targets.append(data['mr_with_pets'])
        for week, boundary in enumerate(result[tbname]['week_boundaries'][1:]):
            try:
                i = result[tbname]['length'].index(boundary)
            except ValueError:
                raise ValueError(
                    'Boundary of week %d, %d, is not found in the data length '
                    'array.' % (week, boundary)
                )
            localData = {
                'predictability': result[tbname]['predictability'],
                'entropy_rate': result[tbname]['entropy_rate']
            }
            for target in targets:
                while len(target) <= week:
                    target.append({
                        'predictability': [],
                        'entropy_rate': []
                    })
                target[week]['predictability'].append(
                    localData['predictability'][i]
                )
                target[week]['entropy_rate'].append(
                    localData['entropy_rate'][i]
                )
    # Calculate statistics
    stats = {}
    for label in data:
        stats[label] = {
            'predictability': {
                'mean': [],
                'std': [],
                'min': [],
                'max': [],
                'n': []
            },
            'entropy_rate': {
                'mean': [],
                'std': [],
                'min': [],
                'max': [],
                'n': []
            }
        }
        for w, dataByWeek in enumerate(data[label]):
            predictabilityArray = dataByWeek['predictability']
            entropyRateArray = dataByWeek['entropy_rate']
            stats[label]['predictability']['mean'].append(
                np.mean(predictabilityArray)
            )
            stats[label]['predictability']['std'].append(
                np.std(predictabilityArray)
            )
            stats[label]['predictability']['min'].append(
                np.min(predictabilityArray)
            )
            stats[label]['predictability']['max'].append(
                np.max(predictabilityArray)
            )
            stats[label]['predictability']['n'].append(
                len(predictabilityArray)
            )
            stats[label]['entropy_rate']['mean'].append(
                np.mean(entropyRateArray)
            )
            stats[label]['entropy_rate']['std'].append(
                np.std(entropyRateArray)
            )
            stats[label]['entropy_rate']['min'].append(
                np.min(entropyRateArray)
            )
            stats[label]['entropy_rate']['max'].append(
                np.max(entropyRateArray)
            )
            stats[label]['entropy_rate']['n'].append(
                len(entropyRateArray)
            )
    # Plot the statistics
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    # Set figures
    fig, [ax1, ax2] = plt.subplots(
        nrows=1, ncols=2, figsize=(20, 8)
    )
    for i, label in enumerate(stats):
        statsItem = stats[label]['predictability']
        ax1.plot(
            np.arange(len(statsItem['mean'])),
            statsItem['mean'],
            color=colors[i],
            label=label
        )
        # ax1.errorbar(
        #     np.arange(len(statsItem['mean'])),
        #     statsItem['mean'],
        #     statsItem['std'],
        #     color=colors[i]
        # )
        # ax1.fill_between(
        #     np.arange(len(statsItem['mean'])),
        #     statsItem['max'],
        #     statsItem['min'],
        #     facecolor=colors[i],
        #     alpha=0.4
        # )
        ax1.fill_between(
            np.arange(len(statsItem['mean'])),
            np.array(statsItem['mean']) + np.array(statsItem['std']),
            np.array(statsItem['mean']) - np.array(statsItem['std']),
            facecolor=colors[i],
            alpha=0.4
        )
    ax1.set_ylabel('Predictability')
    ax1.set_xscale('log')
    ax1.set_xlabel('weeks')
    ax1.legend()
    for i, label in enumerate(stats):
        statsItem = stats[label]['entropy_rate']
        ax2.plot(
            np.arange(len(statsItem['mean'])),
            statsItem['mean'],
            color=colors[i],
            label=label
        )
        # ax2.errorbar(
        #     np.arange(len(statsItem['mean'])),
        #     statsItem['mean'],
        #     statsItem['std'],
        #     color=colors[i]
        # )
        # ax2.fill_between(
        #     np.arange(len(statsItem['mean'])),
        #     statsItem['max'],
        #     statsItem['min'],
        #     facecolor=colors[i],
        #     alpha=0.4
        # )
        ax2.fill_between(
            np.arange(len(statsItem['mean'])),
            np.array(statsItem['mean']) + np.array(statsItem['std']),
            np.array(statsItem['mean']) - np.array(statsItem['std']),
            facecolor=colors[i],
            alpha=0.4
        )
    ax2.set_ylabel('Entropy rate (bits)')
    ax2.set_xlabel('weeks')
    ax2.legend()
    ax2.set_xscale('log')
    plt.show()
