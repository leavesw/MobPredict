import os
import json
import humanize
import argparse
from collections import OrderedDict


def IsSingle(tbname, datasetNameMapDict):
  """Check if the dataset is recorded in a single-resident smart home.

  The information is provided by the name map. Any dataset starts with 'm' or
  'b' in the mapped name is recorded in a multi-resident smart home.

  Args:
    tbname: str, original name of the dataset
    datasetNameMapDict: dict, containing the name map of the datasets, indexed
      by their original names.

  Returns:
    True, if the dataset is recorded in a single-resident smart home;
    False, otherwise.
  """
  mappedName = datasetNameMapDict[tbname]
  if isinstance(mappedName, dict):
    mappedName = mappedName['label']
  if mappedName[0] == 'b' or mappedName[0] == 'm':
    return False
  else:
    return True

def LoadDatasetNameMap(filename):
  """Load name map from a file.

  Args:
      filename: str, path to the smart home name map

  Returns:
    Ordered dictionary of mapped dataset names.
  """
  datasetNameMapDict = OrderedDict()
  fp = open(filename, 'r')
  for line in fp.readlines():
    line = line.strip()
    if len(line) == 0 or line[0] == '#':
      continue
    tokens = line.split()
    print(tokens)
    datasetNameMapDict[tokens[0]] = tokens[1]
  fp.close()
  return datasetNameMapDict


room_types = ['bedrooms', 'bathrooms', 'living rooms', 'kitchen',
              'dining', 'other']


def LoadDatasetRoomInformation(filename):
  """Load dataset testbed room information from file

  Args:
    filename: str, path to the smart home name

  Returns:
    Ordered dictionary of mapped dataset names and room information
  """
  datasetInfoMapDict = OrderedDict()
  fp = open(filename, 'r')
  for line in fp.readlines():
    line = line.strip().replace(',', '')
    if len(line) == 0 or line[0] == '#':
      continue
    tokens = line.split()
    print(tokens)
    if tokens[2] == '?':
      continue
    testbed_name = tokens[0]
    datasetInfoMapDict[testbed_name] = {
      'label': tokens[1]
    }
    datasetInfoMapDict[testbed_name]['size'] = int(tokens[2])
    total_rooms = 0
    for i, room in enumerate(room_types):
      datasetInfoMapDict[testbed_name][room] = int(tokens[3 + i])
      total_rooms += int(tokens[3 + i])
    datasetInfoMapDict[testbed_name]['num_rooms'] = total_rooms
  fp.close()
  return datasetInfoMapDict


def LoadSitesSummary(summaryFile):
  """Load CASAS Smart Home site summaries.

  Args:
    summaryFile: Smart home site information JSON file.

  Returns:
    Dictionary of smart home site indexed by site name.
  """
  if os.path.isfile(summaryFile):
    fp = open(summaryFile, 'r')
    siteInfoList = json.load(fp)
    fp.close()
    siteInfoDict = OrderedDict()
    for site in siteInfoList:
      siteInfoDict[site['name']] = site
    return siteInfoDict
  else:
    raise FileNotFoundError(
      'Cannot find summary file for CASAS sites at %s. (Absolute path: %s)' %
      (summaryFile, os.path.abspath(summaryFile))
    )


def LoadEntropyRateAndProdictability(chkptRoot, tbname, method='est'):
  """Find calculated entropy rate and predictability of a given smart home.

  Args:
    chkptRoot: str, path to the root directory of dataset checkpoints
    tbname: str, original name of the dataset
    method: str, methods used to calculate entropy rate ('est')

  Returns:
    dict, containing the information of the predictability of the dataset.
  """
  if not os.path.isdir(chkptRoot):
    raise NotADirectoryError('Folder %s not found.' % chkptRoot)
  tbChkptPath = os.path.join(chkptRoot, tbname)
  if not os.path.isdir(tbChkptPath):
    raise NotADirectoryError(
      'Checkpoint folder %s for smart home %s not found.' % (
        tbChkptPath, tbname
      )
    )
  entropyFilename = os.path.join(tbChkptPath, 'entropy.json')
  if not os.path.isfile(entropyFilename):
    raise FileNotFoundError(
      'Entropy rate and prediction result not found. Make sure the file '
      '%s for smart home %s exists.' % (entropyFilename, tbname)
    )
  summaryFilename = os.path.join(tbChkptPath, 'summary.json')
  if not os.path.isfile(summaryFilename):
    raise FileNotFoundError(
      'Dataset summary file not found. Make sure that the file '
      '%s for smart home %s exists' % (summaryFilename, tbname)
    )
  fp = open(entropyFilename, 'r')
  dictEntropy = json.load(fp)
  fp.close()
  # if method not in dictEntropy:
  #   raise ValueError(
  #     'Method %s not implemented during the entropy rate estimation of '
  #     'smart home %s.' % (method, tbname)
  #   )
  if method in dictEntropy:
    entropyRate = dictEntropy[method]['entropy_rate'][-1]
    predictability = dictEntropy[method]['predictability'][-1]
    length = dictEntropy[method]['length'][-1]
  else:
    entropyRate = 0.
    predictability = 0.
    length = dictEntropy["lzma"]['length'][-1]
  fp = open(summaryFilename, 'r')
  dictSummary = json.load(fp)
  fp.close()
  return {
    'entropy_rate': entropyRate,
    'predictability': predictability,
    'length': length,
    'num_sensors': dictSummary['num_sensors'],
    'num_weeks': dictSummary['num_weeks']
  }


if __name__ == '__main__':
  parser = argparse.ArgumentParser(
    description='Predictability Scatter Plot'
  )
  parser.add_argument('-i', '--info', type=str,
                      help='Site information json file')
  parser.add_argument('-m', '--map', type=str,
                      help='Dataset map file')
  parser.add_argument('-r', '--room', type=str,
                      help='Dataset Room Information file')
  parser.add_argument('-s', '--sites', type=str, default='',
                      help='Sites to download, separated by comma (,)')
  parser.add_argument('-c', '--chkpt-root', type=str,
                      help='Root directory of checkpoints')
  parser.add_argument('-o', '--output', default='.',
                      help='Directory to store scatter plot files.')
  args = parser.parse_args()
  infoFilename = args.info
  mapFilename = args.map
  roomInfoFilename = args.room
  outputPath = args.output
  sites = str(args.sites)
  chkptRoot = args.chkpt_root

  # Get all sites
  siteInfoDict = LoadSitesSummary(infoFilename)
  datasetNameMapDict = LoadDatasetNameMap(mapFilename)
  datasetInfoMapDict = LoadDatasetRoomInformation(roomInfoFilename)

  # Sites to plot
  if len(sites) == 0:
    sites = list(siteInfoDict.keys())
  else:
    sites = sites.split(',')

  result = {}
  line = 0
  for tbname in datasetNameMapDict:
    if tbname not in sites:
      continue
    try:
      result[tbname] = LoadEntropyRateAndProdictability(
        chkptRoot=chkptRoot, tbname=tbname
      )
    except Exception as error:
      print("Unexpected Error: ", error)
    if tbname in datasetInfoMapDict:
      num_room = humanize.intcomma(datasetInfoMapDict[tbname]["num_rooms"])
      sq_feet = humanize.intcomma(datasetInfoMapDict[tbname]["size"])
    else:
      num_room = 'N/A'
      sq_feet = 'N/A'
    if IsSingle(tbname, datasetNameMapDict):
      num_residents = "1"
    else:
      num_residents = "2-3"
    if line % 2 == 1:
      even_comment = ''
    else:
      even_comment = '&%'
    # site, room, sqfeet, sensors, residents, events, weeks
    print("%s & %s & %s & %s & %s & %s & %s %s\\\\" % (
      datasetNameMapDict[tbname],
      num_room,
      sq_feet,
      humanize.intcomma(result[tbname]['num_sensors']),
      num_residents,
      humanize.intcomma(result[tbname]['length']),
      humanize.intcomma(result[tbname]['num_weeks']),
      even_comment
    ))
    line += 1
