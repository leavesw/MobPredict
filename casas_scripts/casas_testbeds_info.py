"""In this script, I intended to collect and organize information regarding testbeds of interest.

The information automatically gathered here includes time-zone information, start date, end date,
number of sensors. Later-on, humans can fill in the rest of information, such as how many bedrooms,
how many residents, and other descriptions.
"""
import os
import psycopg2
import json
import xlsxwriter
import argparse
from collections import OrderedDict

xlsxFile = 'sites_summary.xlsx'
jsonFile = 'sites_summary.json'

columnList = OrderedDict([
  ('name', 'Site'),
  ('timezone', 'Time Zone'),
  ('isActive', 'Active'),
  ('firstEventUTC', 'First Event (UTC)'),
  ('latestEventUTC', 'Latest Event (UTC)'),
  ('#sensors', '# Sensors'),
  ('#door', '# Door Sensors'),
  ('#motion', '# Motion Sensors'),
  ('#item', '# Item Sensors'),
  ('#residents', '# Residents'),
  ('#bedrooms', '# Bedrooms'),
  ('#bathrooms', '# Bathrooms'),
  ('description', 'Description')
])

# Connection to database
dbHost = 'localhost'
dbPort = 5432
dbUser = 'twang'
dbName = 'smarthomedata'
conn = None


def formatDatetime(time):
  """Format datetime into string with timezone information.

  Args:
    time: datetime, with timezone information
  """
  return time.strftime("%m/%d/%Y %H:%M:%S.%f ") + \
         time.strftime("%z")[:3] + ":" + \
         time.strftime("%z")[3:]


def DbQuerySiteInfo(tb_list):
  """Database query to obtain information of a specific smart home site.

  Args:
    tb_list (:obj:`str`): List of testbed names.

  Return:
    List of testbed information organized, each testbed information is a
    dictionary with the following keys: "name", "description", "isActive",
    "timezone", "first_event", "latest_event".
  """
  tb_conditions = [
    "tbname='%s'" % tbname for tbname in tb_list
  ]
  tb_conditions_string = ' or '.join(tb_conditions)
  cur = conn.cursor()
  cur.execute(
    "select tbname, description, active, timezone, first_event,"
    "latest_event from testbed where %s;" % tb_conditions_string
  )
  rowlist = cur.fetchall()
  testbed_info_list = []
  for row in rowlist:
    firstEvent = row[4]
    latestEvent = row[5]
    testbed_info_list.append({
      'name': row[0],
      'description': row[1],
      'isActive': row[2],
      'timezone': row[3],
      'firstEventUTC': formatDatetime(row[4]),
      'latestEventUTC': formatDatetime(row[5])
    })
  return testbed_info_list


def DbQuerySiteSensors(tbname):
    """
    Args:
        tbname (:obj:`str`): Testbed name.

    Return:
         Array of sensor dictionary.
    """
    sensor_lookup = {}
    sensor_list = []
    cur = conn.cursor()
    cur.execute(
        "select target, sensor_type, serial from detailed_all_sensors "
        "where tbname='%s';" % tbname)
    rowlist = cur.fetchall()
    for row in rowlist:
        if row[0] in sensor_lookup:
            sensor_lookup[row[0]]["types"].append(row[1])
            sensor_lookup[row[0]]["serial"].append(row[2])
        else:
            sensor = {
                "name": row[0],
                "types": [row[1]],
                "serial": [row[2]]
            }
            sensor_list.append(sensor)
            sensor_lookup[row[0]] = sensor
    return sensor_list


# def DbQuerySiteStart(tbname):
#     cur = conn.cursor()
#     cur.execute(
#         "select stamp, target, message, sensor_type from all_events "
#         "where tbname='%s' order by stamp asc limit 1;" % tbname)
#     event = cur.fetchall()
#     print(event)
#     return event[0][0]
#
#
# def DbQuerySiteEnd(tbname):
#     cur = conn.cursor()
#     cur.execute(
#         "select stamp, target, message, sensor_type from all_events "
#         "where tbname='%s' order by stamp desc limit 1;" % tbname)
#     event = cur.fetchall()
#     print(event)
#     return event[0][0]


def get_category(type_name):
  """Obtain sensor category depending on sensor type string.

  Sensor categories include Battery, Error, Radio, Motion, Door, LightSwitch,
  Light, Temperature, Item and Other.

  Args:
      type_name: sensor_type string obtained from database

  Returns:
      corresponding event/sensor categories
  """
  if 'Battery' in type_name:
    return 'Battery'
  if 'Radio_error' in type_name:
    return 'Error'
  if 'Radio' in type_name or 'Zigbee' in type_name:
    return 'Radio'
  if 'Motion' in type_name:
    return 'Motion'
  if 'Door' in type_name:
    return 'Door'
  if 'LightSwitch' in type_name:
    return 'LightSwitch'
  if 'Light' in type_name:
    return 'Light'
  if 'Temperature' in type_name or 'Thermostat' in type_name:
    return 'Temperature'
  if 'Item' in type_name:
    return 'Item'
  return 'Other'


def getSiteList(filename):
  """Get list of sites from text file

  Args:
    filename: str, file containing the list of smart home sites
  """
  fp = open(filename)
  siteList = []
  for line in fp.readlines():
    line = line.strip()
    if line != '':
      siteList.append(line)
  fp.close()
  return siteList


def establishConnection():
  """Establish database connection with global parameters
  """
  global conn
  conn = psycopg2.connect(
    host=dbHost, port=dbPort, database=dbName,
    user=dbUser, password=dbPassword
  )


def gatherSiteSensorInfo(site):
  """Gather sensor information of a specific site

  Args:
    tbname: str, testbed name
  """
  print('Gather site name %s' % site['name'])
  sensors = DbQuerySiteSensors(site['name'])
  site['sensor_details'] = sensors
  # This is sensors defined by serial number
  sensorSerials = []
  for sensor in sensors:
    for serial in sensor['serial']:
      if serial not in sensorSerials:
        sensorSerials.append(serial)
  site['#sensors'] = len(sensorSerials)
  site['#door'] = 0
  site['#motion'] = 0
  site['#item'] = 0
  for sensor in sensors:
    categories = [
      get_category(typeName) for typeName in sensor['types']
    ]
    if 'Door' in categories:
      site['#door'] += 1
    if 'Motion' in categories:
      site['#motion'] += 1
    if 'Item' in categories:
      site['#item'] += 1
  return site


def saveXlsx(filename, siteInfoList):
  """Save site info as Excel file

  Args:
    filename: path to the Excel file
    sitesInfo: list of smart home site information.
  """
  workbook = xlsxwriter.Workbook(filename)
  sheet = workbook.add_worksheet('Summary')
  for c, columnKey in enumerate(columnList):
    sheet.write(0, c, columnList[columnKey])
  r = 1
  for site in siteInfoList:
    for c, columnKey in enumerate(columnList):
      if columnKey in site:
        sheet.write(r, c, site[columnKey])
    r += 1
  workbook.close()


if __name__ == "__main__":
  parser = argparse.ArgumentParser(
    description="Load smart home site info from CASAS database"
  )
  parser.add_argument('siteList', type=str,
                      help='List of sites')
  parser.add_argument('-H', '--host', type=str,
                      help='CASAS database server address (server:port)')
  parser.add_argument('-u', '--user', type=str,
                      help='CASAS database username')
  parser.add_argument('-p', '--password', type=str,
                      help='CASAS database password')
  parser.add_argument('-o', '--output',
                      help='Folder to save the site information')
  args = parser.parse_args()
  # Parse Host and Port
  if ':' in args.host:
    dbHost, dbPort = args.host.split(':')
  else:
    dbHost = args.host
    dbPort = '5432'
  dbUser = args.user
  dbPassword = args.password
  outputPath = args.output
  siteListFilename = args.siteList

  establishConnection()
  siteList = getSiteList(siteListFilename)
  siteInfoList = DbQuerySiteInfo(tb_list=siteList)
  for site in siteInfoList:
    site = gatherSiteSensorInfo(site)
    fp = open(os.path.join(outputPath, jsonFile), 'w')
    json.dump(siteInfoList, fp, indent=4)
    fp.close()
  saveXlsx(os.path.join(outputPath, xlsxFile), siteInfoList)
  conn.close()
