import os
from setuptools import find_packages
from setuptools import setup
from Cython.Build import cythonize

# Package Meta-data
CLASSIFIERS = """\
Development Status :: 2 - Pre-Alpha
Intended Audience :: Developers
Intended Audience :: Science/Research
License :: OSI Approved :: BSD License
Programming Language :: Python
Programming Language :: Python :: 3
Programming Language :: Python :: 3.5
Topic :: Home Automation
Topic :: Scientific/Engineering :: Artificial Intelligence
Topic :: Scientific/Engineering :: Information Analysis
""".splitlines()
NAME = "mobpredict"
DESCRIPTION = "Predictability of Human Mobility in Smart Homes with Binary Sensor Data."
LONG_DESCRIPTION = DESCRIPTION
URL = "https://gitlab.com/leavesw/MobPredict.git"
EMAIL = "tinghui.wang@wsu.edu"
AUTHOR = "Tinghui (Steve) Wang"
LICENSE = "BSD"

here = os.path.abspath(os.path.dirname(__file__))

# Get Version from pyActLearn.version
exec_results = {}
exec(open(os.path.join(here, 'mobpredict/version.py')).read(), exec_results)
version = exec_results['__version__']

# Get Install Requirements
with open(os.path.join(here, 'requirements.txt'), 'r') as f:
    install_requires = f.read().splitlines()


# Where the magic happens:
def do_setup():
    setup(
        name=NAME,
        version=version,
        description=DESCRIPTION,
        long_description=LONG_DESCRIPTION,
        classifiers=CLASSIFIERS,
        author=AUTHOR,
        author_email=EMAIL,
        url=URL,
        license=LICENSE,
        packages=find_packages(exclude=('tests',)),
        entry_points={
            'console_scripts': [
            ]
        },
        install_requires=install_requires,
        ext_modules=cythonize('mobpredict/*.pyx', gdb_debug=True),
        language='c++'
    )


if __name__ == "__main__":
    do_setup()
