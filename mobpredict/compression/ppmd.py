"""Lossless data compression using predictive partial matching and adaptive
coding.

The class calls the implementation by 7-zip tool.
The PPMD (Predictive Partial Matching by Dmitry) is implemented according to
Dmitry Shkarin's PPMdH 2002 with small changes according to 7-zip documentation
(https://www.7-zip.org/7z.html).

References:
    1. Cleary, J. and Witten, I., 1984. Data compression using adaptive coding
       and partial string matching. IEEE transactions on Communications, 32(4),
       pp.396-402.
    2. Shkarin, D., 2002. PPM: One step to practicality. In Data Compression
       Conference, 2002. Proceedings. DCC 2002 (pp. 202-211). IEEE.
"""
import logging
import subprocess
from .config import config

logger = logging.getLogger(__name__)


class PPM:
    """Predictive Partial Matching (PPM) Class.

    The class provides function bindings for 7-zip compression and decompression
    methods.
    """
    @staticmethod
    def compress(inFile, outFile, mem='192M', order=24):
        """Compress file using PPMd algorithm
        
        Args:
            inFile (str): Input file name.
            outFile (str): Output file name.
            mem (str, optional): Defaults to '192M'. Memory allocated for PPMd.
                The maximum value is 2G.
            order (int, optional): Defaults to 24. PPMd model order. The size
                must be in the range [2, 32].
        """
        cmd = [
            config['7z'], 'a', '-t7z', outFile, inFile, '-m0=PPMd:mem=192M:o=24'
        ]
        sp = subprocess.Popen(
            cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE
        )
        data, errs = sp.communicate()
        if sp.returncode != 0:
            if data is None:
                data = b''
            if errs is None:
                errs = b''
            raise ChildProcessError(
                'Failed to execute command %s with error code %d\r\n'
                'Console output:\r\n\t%s\r\n'
                'Errors: \r\n%s\r\n' % (
                    ' '.join(cmd),
                    sp.returncode,
                    '\r\n\t'.join([
                        line for line in data.decode('utf-8').splitlines()
                    ]),
                    '\r\n\t'.join([
                        err for err in errs.decode('utf-8').splitlines()
                    ])
                )
            )
        return sp.returncode == 0
