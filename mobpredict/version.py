MAJOR = 0
MINOR = 2
MICRO = 0
SUFFIX = "dev0"

__version__ = '%d.%d.%d.%s' % (MAJOR, MINOR, MICRO, SUFFIX)
