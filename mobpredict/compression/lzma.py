"""Limpel-Ziv-Markov chain algorithm (LZMA)

The LZMA algorithm is a lossless data compression developed by Igor Pavlov.
LZMA uses a dictionary compression algorithm, a variation of LZ77 with huge
dictionary sizes and special support for repeated used match distances. The
output is then encoded with a range encoder, using a complex model to make a
probability prediction of each bit.

Later, LZMA2 is introduced, which improves the speed of compression in
multi-threaded environment. The LZMA2 will divide the input file into smaller
blocks. Each block is then encoded with LZMA method. The LZMA also allows
un-compressed data blocks to handle already compressed inputs.
"""
import logging
import subprocess
from .config import config

logger = logging.getLogger(__name__)


class LZMA:
    """Limpel-Ziv-Markov chain Algorithm (LZMA)

    The class provides function bindings for 7-zip compression and decompression
    methods.
    """
    def compress(self, inFile, outFile):
        """Compress file using LZMA
        
        Args:
            inFile (str): Input file name.
            outFile (str): Output file name.
            mem (str, optional): Defaults to '192M'. Memory allocated for PPMd.
                The maximum value is 2G.
            order (int, optional): Defaults to 24. PPMd model order. The size
                must be in the range [2, 32].
        """
        cmd = [config['7z'], 'a', '-t7z', outFile, inFile, '-m0=lzma:d=192M']
        sp = subprocess.Popen(
            cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE
        )
        data, errs = sp.communicate()
        if sp.returncode != 0:
            if data is None:
                data = b''
            if errs is None:
                errs = b''
            raise ChildProcessError(
                'Failed to execute command %s with error code %d\r\n'
                'Console output:\r\n\t%s\r\n'
                'Errors: \r\n%s\r\n' % (
                    ' '.join(cmd),
                    sp.returncode,
                    '\r\n\t'.join([
                        line for line in data.decode('utf-8').splitlines()
                    ]),
                    '\r\n\t'.join([
                        err for err in errs.decode('utf-8').splitlines()
                    ])
                )
            )
        return sp.returncode == 0
