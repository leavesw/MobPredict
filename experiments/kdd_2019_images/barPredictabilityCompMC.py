"""Scatter plot of predictability and entropy rate of each smart home, against
the number of sensors deployed in those smart homes.
"""
import os
import json
import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict


def IsSingleResident(tbname):
    """Returns true if there is only one resident staying in the smart home.
    """
    return tbname not in {'hh107', 'hh121', 'tm004', 'kyoto', 'tulum', 'rw104',
                          'rw110', 'cairo', 'paris',
                          'atmo1', 'atmo2', 'atmo4', 'atmo5', 'atmo6', 'atmo7',
                          'atmo8', 'atmo9', 'atmo10'}


def HasPets(tbname):
    """Returns true if the smart home has pets.
    """
    return tbname in {'cairo', 'paris', 'rw103', 'milan'}


def LoadEntropyRateAndProdictability(chkptPath, tbname, method='est'):
    """Find calculated entropy rate and predictability of a given smart home.
    """
    if not os.path.isdir(chkptPath):
        raise NotADirectoryError('Folder %s not found.' % chkptPath)
    tbChkptPath = os.path.join(chkptPath, tbname)
    if not os.path.isdir(tbChkptPath):
        raise NotADirectoryError(
            'Checkpoint folder %s for smart home %s not found.' % (
                tbChkptPath, tbname
            )
        )
    entropyFilename = os.path.join(tbChkptPath, 'entropy.json')
    if not os.path.isfile(entropyFilename):
        raise FileNotFoundError(
            'Entropy rate and prediction result not found. Make sure the file '
            '%s for smart home %s exists.' % (entropyFilename, tbname)
        )
    summaryFilename = os.path.join(tbChkptPath, 'summary.json')
    if not os.path.isfile(summaryFilename):
        raise FileNotFoundError(
            'Dataset summary file not found. Make sure that the file '
            '%s for smart home %s exists' % (summaryFilename, tbname)
        )
    fp = open(entropyFilename, 'r')
    dictEntropy = json.load(fp)
    fp.close()
    if method not in dictEntropy:
        raise ValueError(
            'Method %s not implemented during the entropy rate estimation of '
            'smart home %s.' % (method, tbname)
        )
    entropyRate = dictEntropy[method]['entropy_rate'][-1]
    predictability = dictEntropy[method]['predictability'][-1]
    length = dictEntropy[method]['length'][-1]
    fp = open(summaryFilename, 'r')
    dictSummary = json.load(fp)
    fp.close()
    return {
        'entropy_rate': entropyRate,
        'predictability': predictability,
        'length': length,
        'num_sensors': dictSummary['num_sensors'],
        'num_weeks': dictSummary['num_weeks'],
        'week_boundaries': dictSummary['week_boundaries']
    }


testbeds = [
    # Two or more residents
    'hh107', 'hh121', 'tm004', 'rw110',
    'atmo1', 'atmo2', 'atmo4', 'atmo6', 'atmo7',
    'atmo8', 'atmo9', 'atmo10',
    # Two residents + pets
    'cairo', 'paris',
    # Single resident + pets
    'rw103', 'milan',
    # Single resident
    'hh101', 'hh110',
    'aruba', 'hh102', 'hh103', 'hh104', 'hh105', 'hh106', 'hh108', 'hh109',
    'hh111', 'hh112', 'hh113', 'hh114', 'hh115', 'hh116', 'hh117', 'hh118',
    'hh119', 'hh120', 'hh122', 'hh123', 'hh124', 'hh125', 'hh126',
    'hh127', 'hh128', 'hh129', 'hh130'
]

chkptPath = 'data/chkpt'

if __name__ == '__main__':
    result = {}
    mcResult = {}
    for tbname in testbeds:
        result[tbname] = LoadEntropyRateAndProdictability(
            chkptPath=chkptPath, tbname=tbname
        )
        mcResult[tbname] = LoadEntropyRateAndProdictability(
            chkptPath=chkptPath, tbname=tbname, method='mc'
        )
    # Capture the result
    data = OrderedDict(
        single_resident={
            'label': 'Single Resident\nSmart Homes',
            'mc_predictability': [],
            'predictability': [],
            'entropy_rate': []
        }, multi_resident={
            'label': 'Multi-resident\nSmart Homes',
            'mc_predictability': [],
            'predictability': [],
            'entropy_rate': []
        }, sr_with_pets={
            'label': 'Single Resident\nwith Pets',
            'mc_predictability': [],
            'predictability': [],
            'entropy_rate': []
        }, mr_with_pets={
            'label': 'Multi-resident\nwith Pets',
            'mc_predictability': [],
            'predictability': [],
            'entropy_rate': []
        }
    )
    for tbname in testbeds:
        tbIsSR = IsSingleResident(tbname)
        tbHasPets = HasPets(tbname)
        targets = []
        if tbIsSR:
            targets.append(data['single_resident'])
            if tbHasPets:
                targets.append(data['sr_with_pets'])
        else:
            targets.append(data['multi_resident'])
            if tbHasPets:
                targets.append(data['mr_with_pets'])
        for target in targets:
            target['predictability'].append(
                result[tbname]['predictability']
            )
            target['mc_predictability'].append(
                mcResult[tbname]['predictability']
            )
            target['entropy_rate'].append(
                result[tbname]['entropy_rate']
            )
    # Building Statistics
    stats = OrderedDict()
    for label in data:
        stats[label] = {
            'predictability': {
                'mean': np.mean(data[label]['predictability']),
                'std': np.std(data[label]['predictability']),
                'min': np.min(data[label]['predictability']),
                'max': np.max(data[label]['predictability'])
            },
            'mc_predictability': {
                'mean': np.mean(data[label]['mc_predictability']),
                'std': np.std(data[label]['mc_predictability']),
                'min': np.min(data[label]['mc_predictability']),
                'max': np.max(data[label]['mc_predictability'])
            },
            'entropy_rate': {
                'mean': np.mean(data[label]['entropy_rate']),
                'std': np.std(data[label]['entropy_rate']),
                'min': np.min(data[label]['entropy_rate']),
                'max': np.max(data[label]['entropy_rate'])
            }
        }

    # Plot the statistics
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    barWidth = 0.35
    # Set figures
    fig, ax1 = plt.subplots(
        nrows=1, ncols=1, figsize=(7, 7)
    )
    xlabels = [
        data[label]['label'] for label in stats
    ]
    xticks = np.arange(len(xlabels)) + 1
    predMean = [
        stats[label]['predictability']['mean'] for label in stats
    ]
    predStd = [
        stats[label]['predictability']['std'] for label in stats
    ]
    mcPredMean = [
        stats[label]['mc_predictability']['mean'] for label in stats
    ]
    mcPredStd = [
        stats[label]['mc_predictability']['std'] for label in stats
    ]
    ax1.bar(
        xticks,
        predMean,
        width=barWidth,
        yerr=predStd,
        align='center',
        alpha=0.5,
        capsize=10,
        ecolor='k',
        label='Estimated Predictability'
    )
    ax1.bar(
        xticks + barWidth,
        mcPredMean,
        width=barWidth,
        yerr=mcPredStd,
        align='center',
        alpha=0.5,
        capsize=10,
        ecolor='k',
        label='Markov Chain'
    )
    ax1.set_ylim(0, 1)
    ax1.set_xticks(xticks + barWidth/2)
    ax1.set_xticklabels(xlabels)
    ax1.set_ylabel('Predictability')
    ax1.yaxis.grid(True)
    ax1.legend()
    plt.savefig(
        os.path.join(os.path.dirname(__file__), 'barPredictabilityCompMC.pdf')
    )
    print(json.dumps(stats, indent=4))
